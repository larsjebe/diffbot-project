import os
import re
import json
import glob

import numpy as np
import collections

from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer

STOP = True
STEM = False


MOD_FILE_NUM = 5

TRAIN_DIRECTORY = '../../training_data/train/'


# The function "text_to_wordlist" is from
# https://www.kaggle.com/currie32/quora-question-pairs/the-importance-of-cleaning-text
def sentence_to_wordlist(text, remove_stopwords=False, stem_words=False, complete_sentence=False):
    # Clean the sentence, with the option to remove stopwords and to stem words.
    
    # Convert words to lower case and split them
    text = text.lower().split()

    # Optionally, remove stop words
    if remove_stopwords:
        stops = set(stopwords.words("english"))
        text = [w for w in text if not w in stops]
    
    text = " ".join(text)

    # Clean the text
    
    text = re.sub(r"[^A-Za-z0-9^,!\/'+-=]", " ", text)
    text = re.sub(r"\. ", " ", text)
    text = re.sub(r"\.$", " ", text)
    text = re.sub(r"what's", "what is ", text)
    text = re.sub(r"\'s", " ", text)
    text = re.sub(r"\'ve", " have ", text)
    text = re.sub(r"can't", "cannot ", text)
    text = re.sub(r"n't", " not ", text)
    text = re.sub(r"i'm", "i am ", text)
    text = re.sub(r"\'re", " are ", text)
    text = re.sub(r"\'d", " would ", text)
    text = re.sub(r"\'ll", " will ", text)
    text = re.sub(r",", " ", text)
    #text = re.sub(r"\.", " ", text)
    text = re.sub(r"!", " ! ", text)
    text = re.sub(r"\/", " ", text)
    text = re.sub(r"\^", " ^ ", text)
    text = re.sub(r"\+", " + ", text)
    #text = re.sub(r"\-", " - ", text) #really?
    text = re.sub(r"\s-\s", " ", text) # really?
    text = re.sub(r"\=", " = ", text)
    text = re.sub(r"'", " ", text)
    text = re.sub(r"(\d+)(k)", r"\g<1>000", text)
    text = re.sub(r":", " : ", text)
    text = re.sub(r" e g ", " eg ", text)
    text = re.sub(r" b g ", " bg ", text)
    text = re.sub(r" u s ", " american ", text)
    text = re.sub(r"\0s", "0", text)
    text = re.sub(r" 9 11 ", "911", text)
    text = re.sub(r"e - mail", "email", text)
    text = re.sub(r"j k", "jk", text)
    text = re.sub(r"\s{2,}", " ", text)
   
    
    # Optionally, shorten words to their stems
    if stem_words:
        text = text.split()
        stemmer = SnowballStemmer('english')
        stemmed_words = [stemmer.stem(word) for word in text]
        text = " ".join(stemmed_words)
    
    # Return a list of words
    if complete_sentence:
        return (text)
    else:
        return(text.split())   

def get_complete_dav_triples(filename, MAX_LENGTH = 300, CUSTOM_ATTS = False):
        
    json_data=open(filename)
    data = json.load(json_data)
    json_data.close()
    
    X_d = []
    X_a = []
    y_v = []

    for element in data:
        title = element["title"]
        text = element["text"]

        for attribute in element["specs"]:
            v = sentence_to_wordlist(element["specs"][attribute], remove_stopwords=False, stem_words=STEM)
            if not CUSTOM_ATTS:
                a = sentence_to_wordlist(attribute.replace("_", " "), remove_stopwords=STOP, stem_words=STEM)
            else:
                a = [attribute]
            d = sentence_to_wordlist(title + " " + text, remove_stopwords=STOP, stem_words=STEM)

            if len(d) > MAX_LENGTH:
                d = d[:int(MAX_LENGTH/2)]+d[-int(MAX_LENGTH/2):]                    

            X_d.append(d)
            X_a.append(a)
            y_v.append(v)
            
    return (X_d,X_a,y_v) 

def get_dav_triples(filename, MAX_LENGTH = 300, CUSTOM_ATTS = False):
    
    isTrain = False
    if 'trainExamples.json' in filename: isTrain = True
        
    
    json_data=open(filename)
    data = json.load(json_data)
    json_data.close()
    
    X_d = []
    X_a = []
    y_v = []

    for element in data:
        title = element["title"]
        text = element["text"]

        for attribute in element["specs"]:
            v = sentence_to_wordlist(element["specs"][attribute], remove_stopwords=False, stem_words=STEM)
            if len(v) == 0: continue
            if not CUSTOM_ATTS:
                a = sentence_to_wordlist(attribute.replace("_", " "), remove_stopwords=STOP, stem_words=STEM)
            else:
                a = [attribute]
            d = sentence_to_wordlist(title + " " + text, remove_stopwords=STOP, stem_words=STEM)

            # cut description to MAX_LENGTH first
            if isTrain:
                if len(d) > MAX_LENGTH:
                    d = d[:int(MAX_LENGTH/2)]+d[-int(MAX_LENGTH/2):]                    
                                
            # check if value is predictable
            v_present = [(v_word in d) for v_word in v]
            v_present_len = len(v_present)
            
            if " ".join(v) in " ".join(d) and v_present_len and v_present_len == sum(v_present):
                if len(d) > MAX_LENGTH:
                    d = d[:int(MAX_LENGTH/2)]+d[-int(MAX_LENGTH/2):]
                X_d.append(d)
                X_a.append(a)
                y_v.append(v)
            
    return (X_d,X_a,y_v) 





def get_train_dav_triples(NUM_FILES = 5, MAX_LENGTH = 300, CUSTOM_ATTS = False):
    
    X_d = []
    X_a = []
    y_v = []

    index = 0

    for file in glob.glob(TRAIN_DIRECTORY+'*.json'):
        if (index >= NUM_FILES): break
        index += 1
        if (index % MOD_FILE_NUM == 0): # these files are reserved for test
            continue

        json_data=open(file)
        data = json.load(json_data)
        json_data.close()    

        for element in data:
            title = element["title"]
            text = element["text"]

            for attribute in element["specs"]:
                v = sentence_to_wordlist(element["specs"][attribute], remove_stopwords=False, stem_words=STEM)
                if len(v) == 0: continue
                if not CUSTOM_ATTS:
                    a = sentence_to_wordlist(attribute.replace("_", " "), remove_stopwords=STOP, stem_words=STEM)
                else:
                    a = [attribute]
                d = sentence_to_wordlist(title + " " + text, remove_stopwords=STOP, stem_words=STEM)

                # cut description to MAX_LENGTH
                if len(d) > MAX_LENGTH:
                    d = d[:int(MAX_LENGTH/2)]+d[-int(MAX_LENGTH/2):]
                
                # check if value is predictable
                v_present = [(v_word in d) for v_word in v]
                v_present_len = len(v_present)

                if " ".join(v) in " ".join(d) and v_present_len and v_present_len == sum(v_present):
                    X_d.append(d)
                    X_a.append(a)
                    y_v.append(v)

    return (X_d,X_a,y_v)

def get_test_dav_triples(NUM_FILES = 5, MAX_LENGTH = 300, CUSTOM_ATTS = False):
    
    X_d = []
    X_a = []
    y_v = []

    index = 0

    for file in glob.glob(TRAIN_DIRECTORY+'*.json'):
        if (index >= NUM_FILES): break
        index += 1
        if (index % MOD_FILE_NUM != 0): # these files are reserved for test
            continue

        json_data=open(file)
        data = json.load(json_data)
        json_data.close()    

        for element in data:
            title = element["title"]
            text = element["text"]

            for attribute in element["specs"]:
                v = sentence_to_wordlist(element["specs"][attribute], remove_stopwords=False, stem_words=STEM)
                if len(v) == 0: continue
                if not CUSTOM_ATTS:
                    a = sentence_to_wordlist(attribute.replace("_", " "), remove_stopwords=STOP, stem_words=STEM)
                else:
                    a = [attribute]
                d = sentence_to_wordlist(title + " " + text, remove_stopwords=STOP, stem_words=STEM)

                # check if value is predictable
                v_present = [(v_word in d) for v_word in v]
                v_present_len = len(v_present)
                if " ".join(v) in " ".join(d) and v_present_len and v_present_len == sum(v_present):
                    # cut description to MAX_LENGTH
                    if len(d) > MAX_LENGTH:
                        d = d[:int(MAX_LENGTH/2)]+d[-int(MAX_LENGTH/2):]

                    X_d.append(d)
                    X_a.append(a)
                    y_v.append(v)

    return (X_d,X_a,y_v)

def create_att_index():
    
    att_index = {}
    index = 1
    
    for file in glob.glob(TRAIN_DIRECTORY+'*.json'):
        print(file)
        json_data=open(file)
        data = json.load(json_data)
        json_data.close()    

        for element in data:
            for a in element["specs"]:
                if a not in att_index:
                    att_index[a] = index
                    index += 1
                    
    # save the attribute index
    filename = 'att_index'
    if not os.path.exists(filename):
        print ('Saving new attribute index to file...')
        np.save(filename, att_index)
        
def create_value_dicts():
    index = 0    
    att_val_count_map = collections.defaultdict(dict)

    for file in glob.glob(TRAIN_DIRECTORY+'*.json'):
        index += 1
        if (index % MOD_FILE_NUM == 0): # these files are reserved for test
            continue
            
        print(file)
        json_data=open(file)
        data = json.load(json_data)
        json_data.close()  
            
        for element in data:
            for a in element["specs"]:
                v = sentence_to_wordlist(element["specs"][a], remove_stopwords=False, stem_words=STEM)
                if tuple(v) not in att_val_count_map[a]:
                    att_val_count_map[a][tuple(v)] = 0
                att_val_count_map[a][tuple(v)] += 1
                
    filename = 'att_val_count_map'
    if not os.path.exists(filename):
        print ('Saving new attribute index to file...')
        np.save(filename, att_val_count_map)
 
