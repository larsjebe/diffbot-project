#### ATTN2 - Incorporating Feedback ####

import data_parser

import os

import numpy as np
from numpy import linalg as LA
np.random.seed(1337)
import collections

from gensim.models import Word2Vec
from gensim.models import KeyedVectors

from keras.layers import Embedding, Dropout, Dense, LSTM, Bidirectional, Input, dot, concatenate, add, Flatten, Activation, multiply

from keras.preprocessing import sequence
from keras.models import load_model
from keras.models import Sequential, Model

from keras.optimizers import Adam

from keras.backend import sigmoid

from  more_itertools import unique_everseen

from sklearn.preprocessing import normalize

BASE_DIR = '../../Google_pretrained_Word2vec/'
EMBEDDING_FILE = BASE_DIR + 'GoogleNews-vectors-negative300.bin'
TRAIN_DATA_FILE = BASE_DIR + 'trainExamples.json'

#### PARAMETERS ####

LSTM_UNITS = 64
MAX_SEQUENCE_LENGTH = 150
MAX_ATTRIBUTE_LENGTH = 1
EPOCHS = 30
BATCH_SIZE = 32
TOY_DATASET = False
NUMBER_FILES = 30

MODEL_NAME = 'ATTN2_L64_SEQ150_E30_B32_30F'

# get training data
if TOY_DATASET:
    d_raw, a_raw, v_raw = data_parser.get_dav_triples(TRAIN_DATA_FILE, MAX_LENGTH=MAX_SEQUENCE_LENGTH, CUSTOM_ATTS = True)

else:
    d_raw, a_raw, v_raw = \
    data_parser.get_train_dav_triples( \
    NUM_FILES = NUMBER_FILES, MAX_LENGTH = MAX_SEQUENCE_LENGTH, CUSTOM_ATTS = True)
    

# create vocab from data
vocab_data = [j for i in d_raw+a_raw+v_raw for j in i]
vocab_data = list(unique_everseen(vocab_data))
att_vocab = [i[0] for i in a_raw]
att_vocab = list(unique_everseen(att_vocab))

# load model
model = KeyedVectors.load_word2vec_format(EMBEDDING_FILE, binary=True)
vocab_goog = list(model.vocab.keys())

#create detmerinistically ordered vocab
set_goog = frozenset(vocab_goog)
vocab_inters = [x for x in vocab_data if x in set_goog]
part_goog = vocab_goog[:100000]
vocab_used = list(unique_everseen(vocab_inters + part_goog))

print ('Length of used vocab: %d' % (len(vocab_used)))
vocab_length = len(vocab_used)

#index vocab
word_index = collections.defaultdict(int)
for i,word in enumerate(vocab_used):
    word_index[word] = i+1 # 0 reserved for Unk
    
#load attribute index
att_index = np.load('att_index.npy').item() 
att_index_len = len(att_index)
    
# save the word index for later use
filename = 'word_index_' + MODEL_NAME
if not os.path.exists(filename):
    print ('Saving new word index to file...')
    np.save(filename, word_index)

# vectorize words
d = [[word_index[word] for word in desc] for desc in d_raw]
a = [att_index[word[0]] for word in a_raw]
num_examples = len(a)

# get position of value in text (without padding)
v_start_idx = [d_raw[ex_idx].index(value[0]) if (len(value) > 0 and value[0] in d_raw[ex_idx]) \
               else -1 for ex_idx, value in enumerate(v_raw)]
v_end_idx = [d_raw[ex_idx].index(value[-1]) if (len(value) > 0 and value[-1] in d_raw[ex_idx]) \
             else -1 for ex_idx, value in enumerate(v_raw)]

#shift position by padding (one number per example)
train_shifts = [MAX_SEQUENCE_LENGTH-len(d_raw[ex_idx]) for ex_idx, _ in enumerate(v_raw)]
v_start_idx_shifted = [pos + train_shifts[ex_idx] for ex_idx, pos in enumerate(v_start_idx)]
v_end_idx_shifted = [pos + train_shifts[ex_idx] for ex_idx, pos in enumerate(v_end_idx)]

# convert to one-hot
v_start = np.zeros((num_examples, MAX_SEQUENCE_LENGTH))
for i in range(num_examples):
    v_start[i][v_start_idx_shifted[i]] = 1

v_end = np.zeros((num_examples, MAX_SEQUENCE_LENGTH))
for i in range(num_examples):
    v_end[i][v_end_idx_shifted[i]] = 1
                     
        
# pad input data
d = sequence.pad_sequences(d, maxlen=MAX_SEQUENCE_LENGTH)
a = np.array(a)

#### CREATE MODEL ####
# create embedding matrix
EMBEDDING_DIM = 300 # length of word vectors in google-embedding
    
embedding_matrix = np.zeros((vocab_length + 1, EMBEDDING_DIM))

for word, idx in word_index.items():
    if word not in model: # TODO: why does this happen
        continue
    embedding_matrix[idx] = model[word]
    
# description encoder
d_in = Input(shape = (MAX_SEQUENCE_LENGTH,), name='d_in')

d_enc = Embedding(output_dim=EMBEDDING_DIM, input_dim=vocab_length+1,
                   input_length=MAX_SEQUENCE_LENGTH,
                   weights=[embedding_matrix], mask_zero=True, trainable=False)(d_in)

d_enc = LSTM(LSTM_UNITS)(d_enc)
#d_enc = Dropout(0.3)(d_enc)


# attribute encoder
a_in = Input(shape = (1,), name='a_in')

a_enc = Embedding(output_dim=LSTM_UNITS, input_dim=att_index_len+1,
                   input_length=1, trainable=True)(a_in)
a_enc = Flatten()(a_enc)


# merge description and attribute => facts
# output shape: (None, MAX_SEQUENCE_LENGTH, MAX_ATTRIBUTE_LENGTH)
# compose a "submodel" that contains all the layers that are shared across your N models

#### Attention Gate (does not train well)
#a_enc = Activation(sigmoid)(a_enc)
#dotted = multiply([d_enc, a_enc], name='dotted') 

dotted = concatenate([d_enc, a_enc], name='dotted')

dense_start = Dense(MAX_SEQUENCE_LENGTH, activation="softmax", name='dense_start')(dotted)
dense_end = Dense(MAX_SEQUENCE_LENGTH, activation="softmax", name='dense_end')(dotted)

model = Model(inputs=[d_in, a_in], outputs=[dense_start, dense_end])

#define optimizer
opt = Adam()

# build the model
model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])


print ('Training model...')
model.fit([d, a], [v_start, v_end], epochs=30, batch_size=32, verbose=1)

print('Saving Model...')
model.save(MODEL_NAME + '.h5')
    
