#### ATTN3 ####

import data_parser
import predict3_function as p3f

import os

import numpy as np
from numpy import linalg as LA
np.random.seed(1337)
import collections

from gensim.models import Word2Vec
from gensim.models import KeyedVectors

from keras.layers import Embedding, Dropout, Dense, LSTM, Bidirectional, Input, dot, concatenate, add, Flatten, Activation, multiply, Add, Concatenate, Lambda

from keras.preprocessing import sequence
from keras.models import load_model
from keras.models import Sequential, Model
from keras.optimizers import Adam
from keras.backend import sigmoid, softmax
from keras.backend import l2_normalize

from  more_itertools import unique_everseen

from sklearn.preprocessing import normalize

BASE_DIR = '../../Google_pretrained_Word2vec/'
EMBEDDING_FILE = BASE_DIR + 'GoogleNews-vectors-negative300.bin'
TRAIN_DATA_FILE = BASE_DIR + 'trainExamples.json'

#### PARAMETERS ####

LSTM_UNITS = 80
MAX_SEQUENCE_LENGTH = 2*LSTM_UNITS
MAX_ATTRIBUTE_LENGTH = 1
MAX_VALUE_LENGTH = 10
EPOCHS = 1
BATCH_SIZE = 32
TOY_DATASET = True
NUMBER_FILES = 30

MODEL_NAME = 'ATTN3_SEQ160_E5_B32_normavg_Toy'

# get training data
if TOY_DATASET:
    d_raw, a_raw, v_raw = data_parser.get_dav_triples(TRAIN_DATA_FILE, MAX_LENGTH=MAX_SEQUENCE_LENGTH, CUSTOM_ATTS = True)

else:
    d_raw, a_raw, v_raw = \
    data_parser.get_train_dav_triples( \
    NUM_FILES = NUMBER_FILES, MAX_LENGTH = MAX_SEQUENCE_LENGTH, CUSTOM_ATTS = True)
    
# index all possible tuples in 0 to (MAX_SEQUENCE_LENGTH-1) that are MAX_VALUE_LENGTH-1 or less apart
tuple_dict = {}
index = 0
for i in range(MAX_SEQUENCE_LENGTH):
    for j in range(MAX_VALUE_LENGTH):
        if i+j < MAX_SEQUENCE_LENGTH:
            tuple_dict[(i,i+j)] = index
            index += 1

# create vocab from data
vocab_data = [j for i in d_raw+a_raw+v_raw for j in i]
vocab_data = list(unique_everseen(vocab_data))
att_vocab = [i[0] for i in a_raw]
att_vocab = list(unique_everseen(att_vocab))

# load model
model = KeyedVectors.load_word2vec_format(EMBEDDING_FILE, binary=True)
vocab_goog = list(model.vocab.keys())

#create detmerinistically ordered vocab
set_goog = frozenset(vocab_goog)
vocab_inters = [x for x in vocab_data if x in set_goog]
part_goog = vocab_goog[:100000]
vocab_used = list(unique_everseen(vocab_inters + part_goog))

print ('Length of used vocab: %d' % (len(vocab_used)))
vocab_length = len(vocab_used)

#index vocab
word_index = collections.defaultdict(int)
for i,word in enumerate(vocab_used):
    word_index[word] = i+1 # 0 reserved for Unk
    
#load attribute index
att_index = np.load('att_index.npy').item() 
att_index_len = len(att_index)
    
#load attribute-value-map index
att_val_map = np.load('att_val_count_map.npy').item() # its a dict of tuples (key is string)
    
# save the word index for later use
filename = 'word_index_' + MODEL_NAME
if not os.path.exists(filename):
    print ('Saving new word index to file...')
    np.save(filename, word_index)

# vectorize words
d = [[word_index[word] for word in desc] for desc in d_raw]
a = [att_index[word[0]] for word in a_raw]
num_examples = len(a)



#shift position by padding (one number per example)
train_shifts = [MAX_SEQUENCE_LENGTH-len(d_raw[ex_idx]) for ex_idx, _ in enumerate(v_raw)]

# create recommendation vectors from data and att_val_map
rec_val = np.zeros((num_examples, len(tuple_dict)))

for i in range(num_examples):
    best_fit = 0
    start = -1
    if a_raw[i][0] in att_val_map:
        possible_values = att_val_map[a_raw[i][0]] # returns dict of value tuple --> count
        for j, word in enumerate(d_raw[i]):
            for val, count in possible_values.items():
                pointer = 0
                while (pointer < len(val) and j+pointer < len(d_raw[i])):
                    if d_raw[i][j+pointer] == val[pointer]:
                        pointer += 1
                    else:
                        break
                if pointer == len(val) and len(val) > 0:
                    if best_fit < count:
                        best_fit = count
                        end = j + pointer-1 + train_shifts[i]
                        start = j + train_shifts[i]
    # for loop
    if start >= 0:
        rec_val[i][tuple_dict[(start, end)]] = 1
        
# create gournd truth tuples in one-hot based on the tuple_dict
v_tuples = np.zeros((num_examples, len(tuple_dict)))
for i in range(num_examples):
    start = -1
    for j, word in enumerate(d_raw[i]):
        pointer = 0
        while (pointer < len(v_raw[i]) and j+pointer < len(d_raw[i])):
            if d_raw[i][j+pointer] == v_raw[i][pointer]:
                pointer += 1
            else:
                break
        if pointer == len(v_raw[i]) and len(v_raw[i]) > 0:
            end = j + pointer-1 + train_shifts[i]
            start = j + train_shifts[i]
            v_tuples[i][tuple_dict[(start, end)]] = 1
            break
                        
        
# pad input data
d = sequence.pad_sequences(d, maxlen=MAX_SEQUENCE_LENGTH)
a = np.array(a)

#### CREATE MODEL ####
# create embedding matrix
EMBEDDING_DIM = 300 # length of word vectors in google-embedding
    
embedding_matrix = np.zeros((vocab_length + 1, EMBEDDING_DIM))

for word, idx in word_index.items():
    if word not in model: # TODO: why does this happen
        continue
    embedding_matrix[idx] = model[word]
    
# description encoder
d_in = Input(shape = (MAX_SEQUENCE_LENGTH,), name='d_in')

d_enc = Embedding(output_dim=EMBEDDING_DIM, input_dim=vocab_length+1,
                   input_length=MAX_SEQUENCE_LENGTH,
                   weights=[embedding_matrix], mask_zero=True, trainable=False)(d_in)

d_enc = LSTM(LSTM_UNITS)(d_enc)
#d_enc = Dropout(0.3)(d_enc)


# attribute encoder
a_in = Input(shape = (1,), name='a_in')

EMBEDDING_DIMENSION = len(tuple_dict)-LSTM_UNITS
a_enc = Embedding(output_dim=EMBEDDING_DIMENSION, input_dim=att_index_len+1,
                   input_length=1, trainable=True)(a_in)
a_enc = Flatten()(a_enc)


# merge description and attribute => facts
# output shape: (None, MAX_SEQUENCE_LENGTH, MAX_ATTRIBUTE_LENGTH)
# compose a "submodel" that contains all the layers that are shared across your N models

concat = Concatenate()([d_enc, a_enc])
concat_n = Lambda(lambda  x: l2_normalize(x,axis=None))(concat)

rec_in = Input(shape = (len(tuple_dict),), name='rec_in')
rec_n = Lambda(lambda  x: l2_normalize(x,axis=None))(rec_in)

added_tensor = Add()([rec_n, concat_n])
added_tensor_n = Lambda(lambda  x: l2_normalize(x,axis=None))(added_tensor)

sm = Activation(softmax)(added_tensor_n)

model = Model(inputs=[d_in, a_in, rec_in], outputs=sm)

'''
rec_in = Input(shape = (len(tuple_dict),), name='rec_in')

sm = Activation(softmax)(rec_in)

model = Model(inputs=rec_in, outputs=sm)
'''

#define optimizer
opt = Adam()

# build the model
model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])

print ('Training model...')
model.fit([d, a, rec_val], v_tuples, epochs=EPOCHS, batch_size=32, verbose=1)

print('Saving Model...')
model.save(MODEL_NAME + '.h5')
    
p3f.predict3_func(model)