import data_parser

import os

import numpy as np
from numpy import linalg as LA
np.random.seed(1337)
import collections

from gensim.models import Word2Vec
from gensim.models import KeyedVectors

from keras.layers import Embedding, Dropout, Dense, LSTM, Bidirectional, Input, dot, concatenate, add

from keras.preprocessing import sequence
from keras.models import load_model
from keras.models import Sequential, Model

from keras.optimizers import Adam

from  more_itertools import unique_everseen

from sklearn.preprocessing import normalize

BASE_DIR = '../../Google_pretrained_Word2vec/'
EMBEDDING_FILE = BASE_DIR + 'GoogleNews-vectors-negative300.bin'
TRAIN_DATA_FILE = BASE_DIR + 'trainExamples.json'

#### PARAMETERS ####

LSTM_UNITS = 100
MAX_SEQUENCE_LENGTH = 150
MAX_ATTRIBUTE_LENGTH = 10
EPOCHS = 30
BATCH_SIZE = 32
TOY_DATASET = True
NUMBER_FILES = 30

MODEL_NAME = 'ATTN_L100Bi_SEQ150_E30_B32_Toy'

# get training data
if TOY_DATASET:
    d_raw, a_raw, v_raw = data_parser.get_dav_triples(TRAIN_DATA_FILE, MAX_LENGTH=MAX_SEQUENCE_LENGTH)

else:
    d_raw, a_raw, v_raw = \
    data_parser.get_train_dav_triples( \
    NUM_FILES = NUMBER_FILES, MAX_LENGTH = MAX_SEQUENCE_LENGTH)
    

# create vocab from data
vocab_data = [j for i in d_raw+a_raw+v_raw for j in i]
vocab_data = list(unique_everseen(vocab_data))

# load model
model = KeyedVectors.load_word2vec_format(EMBEDDING_FILE, binary=True)
vocab_goog = list(model.vocab.keys())

#create detmerinistically ordered vocab
set_goog = frozenset(vocab_goog)
vocab_inters = [x for x in vocab_data if x in set_goog]
part_goog = vocab_goog[:100000]
vocab_used = list(unique_everseen(vocab_inters + part_goog))

print ('Length of used vocab: %d' % (len(vocab_used)))
vocab_length = len(vocab_used)

#index vocab
word_index = collections.defaultdict(int)
for i,word in enumerate(vocab_used):
    word_index[word] = i+1 # 0 reserved for Unk
    
# save the word index for later use
filename = 'word_index_' + MODEL_NAME
if not os.path.exists(filename):
    print ('Saving new word index to file...')
    np.save(filename, word_index)

# vectorize words
d = [[word_index[word] for word in desc] for desc in d_raw]
a = [[word_index[word] for word in att] for att in a_raw]
num_examples = len(a)

# get position of value in text (without padding)
v_start_idx = [d_raw[ex_idx].index(value[0]) if (len(value) > 0 and value[0] in d_raw[ex_idx]) \
               else -1 for ex_idx, value in enumerate(v_raw)]
v_end_idx = [d_raw[ex_idx].index(value[-1]) if (len(value) > 0 and value[-1] in d_raw[ex_idx]) \
             else -1 for ex_idx, value in enumerate(v_raw)]

#shift position by padding (one number per example)
train_shifts = [MAX_SEQUENCE_LENGTH-len(d_raw[ex_idx]) for ex_idx, _ in enumerate(v_raw)]
v_start_idx_shifted = [pos + train_shifts[ex_idx] for ex_idx, pos in enumerate(v_start_idx)]
v_end_idx_shifted = [pos + train_shifts[ex_idx] for ex_idx, pos in enumerate(v_end_idx)]

# convert to one-hot
v_start = np.zeros((num_examples, MAX_SEQUENCE_LENGTH))
for i in range(num_examples):
    v_start[i][v_start_idx_shifted[i]] = 1

v_end = np.zeros((num_examples, MAX_SEQUENCE_LENGTH))
for i in range(num_examples):
    v_end[i][v_end_idx_shifted[i]] = 1
                     
        
# pad input data
d = sequence.pad_sequences(d, maxlen=MAX_SEQUENCE_LENGTH)
a = sequence.pad_sequences(a, maxlen=MAX_ATTRIBUTE_LENGTH)

#### CREATE MODEL ####
# create embedding matrix
EMBEDDING_DIM = 300 # length of word vectors in google-embedding
    
embedding_matrix = np.zeros((vocab_length + 1, EMBEDDING_DIM))

for word, idx in word_index.items():
    if word not in model: # TODO: why does this happen
        continue
    embedding_matrix[idx] = model[word]
    
# description encoder
d_in = Input(shape = (MAX_SEQUENCE_LENGTH,), name='d_in')

d_enc = Embedding(output_dim=EMBEDDING_DIM, input_dim=vocab_length+1,
                   input_length=MAX_SEQUENCE_LENGTH,
                   weights=[embedding_matrix], mask_zero=True, trainable=False)(d_in)

d_enc = Bidirectional(LSTM(LSTM_UNITS))(d_enc)
#d_enc = Dropout(0.3)(d_enc)


# attribute encoder
a_in = Input(shape = (MAX_ATTRIBUTE_LENGTH,), name='a_in')

a_enc = Embedding(output_dim=EMBEDDING_DIM, input_dim=vocab_length+1,
                   input_length=MAX_ATTRIBUTE_LENGTH,
                   weights=[embedding_matrix], mask_zero=True, trainable=False)(a_in)

a_enc = LSTM(LSTM_UNITS)(a_enc)
#a_enc = Dropout(0.3)(a_enc)

# merge description and attribute => facts
# output shape: (None, MAX_SEQUENCE_LENGTH, MAX_ATTRIBUTE_LENGTH)
# compose a "submodel" that contains all the layers that are shared across your N models

#dotted = dot([d_enc, a_enc], axes=(1,1), name='dotted')
dotted = concatenate([d_enc, a_enc], name='dotted')

dense_start = Dense(MAX_SEQUENCE_LENGTH, activation="softmax", name='dense_start')(dotted)
dense_end = Dense(MAX_SEQUENCE_LENGTH, activation="softmax", name='dense_end')(dotted)

model = Model(inputs=[d_in, a_in], outputs=[dense_start, dense_end])

#define optimizer
opt = Adam()

# build the model
model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])


print ('Training model...')
model.fit([d, a], [v_start, v_end], epochs=30, batch_size=32, verbose=1)

print('Saving Model...')
model.save(MODEL_NAME + '.h5')
    
