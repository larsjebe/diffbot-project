import data_parser

import numpy as np
import collections

from gensim.models import Word2Vec
from gensim.models import KeyedVectors

from keras.preprocessing import sequence
from keras.models import load_model

from  more_itertools import unique_everseen

BASE_DIR = '../../Google_pretrained_Word2vec/'
EMBEDDING_FILE = BASE_DIR + 'GoogleNews-vectors-negative300.bin'
TRAIN_DATA_FILE = BASE_DIR + 'trainExamples.json'
TEST_DATA_FILE = BASE_DIR + 'devExamples.json'

TRAIN_ERROR = False
TEST_ERROR = True

TOY_DATASET = False
NUMBER_FILES = 30
MAX_SEQUENCE_LENGTH = 150
MAX_ATTRIBUTE_LENGTH = 5

MODEL_NAME = 'ATTN2_L64_SEQ150_E30_B32_30F'


# load word index from google vocab
word_index = np.load('word_index_'+MODEL_NAME+'.npy').item() 
print ('Length of used vocab: %d' % (len(word_index)))

#load attribute index
att_index = np.load('att_index.npy').item() 
att_index_len = len(att_index)

#load attribute-value-map index
att_val_map = np.load('att_val_map.npy').item() # its a dict of tuples (key is string)

# get training data
if TOY_DATASET:
    if TRAIN_ERROR:
        d_raw, a_raw, v_raw = data_parser.get_dav_triples(TRAIN_DATA_FILE, MAX_LENGTH=MAX_SEQUENCE_LENGTH, CUSTOM_ATTS = True)
    if TEST_ERROR:
        td_raw, ta_raw, tv_raw = data_parser.get_dav_triples(TEST_DATA_FILE, MAX_LENGTH=MAX_SEQUENCE_LENGTH, CUSTOM_ATTS = True)

else:
    if TRAIN_ERROR:
        d_raw, a_raw, v_raw = \
        data_parser.get_train_dav_triples( \
        NUM_FILES = NUMBER_FILES, MAX_LENGTH = MAX_SEQUENCE_LENGTH, CUSTOM_ATTS = True)
    if TEST_ERROR:
        td_raw, ta_raw, tv_raw = \
        data_parser.get_test_dav_triples( \
        NUM_FILES = NUMBER_FILES, MAX_LENGTH = MAX_SEQUENCE_LENGTH, CUSTOM_ATTS = True) 
        
        
if TRAIN_ERROR:
    # vectorize words
    d = [[word_index[word] for word in desc] for desc in d_raw]
    a = [att_index[word[0]] for word in a_raw]
    num_examples = len(a)

    # get position of value in text (without padding)
    v_start_idx = [d_raw[ex_idx].index(value[0]) if (len(value) > 0 and value[0] in d_raw[ex_idx]) \
                   else -1 for ex_idx, value in enumerate(v_raw)]
    v_end_idx = [d_raw[ex_idx].index(value[-1]) if (len(value) > 0 and value[-1] in d_raw[ex_idx]) \
                 else -1 for ex_idx, value in enumerate(v_raw)]

    #shift position by padding (one number per example)
    train_shifts = [MAX_SEQUENCE_LENGTH-len(d_raw[ex_idx]) for ex_idx, _ in enumerate(v_raw)]
    v_start_idx_shifted = [pos + train_shifts[ex_idx] for ex_idx, pos in enumerate(v_start_idx)]
    v_end_idx_shifted = [pos + train_shifts[ex_idx] for ex_idx, pos in enumerate(v_end_idx)]

    # convert to one-hot
    v_start = np.zeros((num_examples, MAX_SEQUENCE_LENGTH))
    for i in range(num_examples):
        v_start[i][v_start_idx_shifted[i]] = 1

    v_end = np.zeros((num_examples, MAX_SEQUENCE_LENGTH))
    for i in range(num_examples):
        v_end[i][v_end_idx_shifted[i]] = 1


    # pad input data
    d = sequence.pad_sequences(d, maxlen=MAX_SEQUENCE_LENGTH)
    a = np.array(a)
    
if TEST_ERROR:
    # vectorize words
    td = [[word_index[word] for word in desc] for desc in td_raw]
    ta = [att_index[word[0]] for word in ta_raw]
    num_examples = len(ta)

    # get position of value in text (without padding)
    tv_start_idx = [td_raw[ex_idx].index(value[0]) if (len(value) > 0 and value[0] in td_raw[ex_idx]) \
                   else -1 for ex_idx, value in enumerate(tv_raw)]
    tv_end_idx = [td_raw[ex_idx].index(value[-1]) if (len(value) > 0 and value[-1] in td_raw[ex_idx]) \
                 else -1 for ex_idx, value in enumerate(tv_raw)]

    #shift position by padding (one number per example)
    test_shifts = [MAX_SEQUENCE_LENGTH-len(td_raw[ex_idx]) for ex_idx, _ in enumerate(tv_raw)]
    tv_start_idx_shifted = [pos + test_shifts[ex_idx] for ex_idx, pos in enumerate(tv_start_idx)]
    tv_end_idx_shifted = [pos + test_shifts[ex_idx] for ex_idx, pos in enumerate(tv_end_idx)]

    # convert to one-hot
    tv_start = np.zeros((num_examples, MAX_SEQUENCE_LENGTH))
    for i in range(num_examples):
        tv_start[i][tv_start_idx_shifted[i]] = 1

    tv_end = np.zeros((num_examples, MAX_SEQUENCE_LENGTH))
    for i in range(num_examples):
        tv_end[i][tv_end_idx_shifted[i]] = 1

    # pad input data
    td = sequence.pad_sequences(td, maxlen=MAX_SEQUENCE_LENGTH)
    ta = np.array(ta)
    
    
model = load_model(MODEL_NAME + '.h5')


if TRAIN_ERROR:
    total_count_train = 0
    correct_count_train = 0
    correct_indices_train = []
    correct_count_mult_train = 0
    total_count_mult_train = 0
    [softmax_start, softmax_end] = model.predict([np.squeeze(d[:,None]), np.squeeze(a[:,None])])
    position_start = [softmax_start[i][0:].argmax() for i in range(np.shape(softmax_start)[0])]
    position_end = [softmax_end[i][0:].argmax() for i in range(np.shape(softmax_end)[0])]
    position_start_shifted = [softmax_start[i][0:].argmax()-train_shifts[i] for i in range(np.shape(softmax_start)[0])]
    position_end_shifted = [softmax_end[i][0:].argmax()-train_shifts[i] for i in range(np.shape(softmax_end)[0])]
    
    num_examples = len(position_start)
    
    for i in range(num_examples):
        if a_raw[i][0] in att_val_map:
            possible_values = att_val_map[a_raw[i][0]] # returns list of tuples
            for j, word in enumerate(d_raw[i]):
                for val in possible_values:
                    pointer = 0
                    while (pointer < len(val) and j+pointer < len(d_raw[i])):
                        if d_raw[i][j+pointer] == val[pointer]:
                            pointer += 1
                        else:
                            break
                    if pointer == len(val):
                        prediction = list(val)            
        else:                
            start = position_start_shifted[i]
            if start < 0: start = 0
            end = position_end_shifted[i]
            if start < end:
                prediction = d_raw[i][start:end+1]
            else:
                prediction = [d_raw[i][start]]
        if prediction == v_raw[i]:
            correct_count_train += 1
            correct_indices_train += [i]
            if len(prediction) > 1:
                correct_count_mult_train += 1
        total_count_train += 1
        if len(v_raw[i]) > 1:
            total_count_mult_train += 1        
    
    print('train accuracy in %:', float(correct_count_train)/total_count_train*100)
    print('train accuracy on multiple word values in %:', float(correct_count_mult_train)/total_count_mult_train*100)
    
if TEST_ERROR:
    total_count_test = 0
    correct_count_test = 0
    correct_indices_test = []
    correct_count_mult_test = 0
    total_count_mult_test = 0
    [softmax_start, softmax_end] = model.predict([np.squeeze(td[:,None]), np.squeeze(ta[:,None])])
    position_start = [softmax_start[i][0:].argmax() for i in range(np.shape(softmax_start)[0])]
    position_end = [softmax_end[i][0:].argmax() for i in range(np.shape(softmax_end)[0])]
    position_start_shifted = [softmax_start[i][0:].argmax()-test_shifts[i] for i in range(np.shape(softmax_start)[0])]
    position_end_shifted = [softmax_end[i][0:].argmax()-test_shifts[i] for i in range(np.shape(softmax_end)[0])]
    
    num_examples = len(position_start)
    
    predictions = {}
    
    for i in range(num_examples):
        if ta_raw[i][0] in att_val_map:
            possible_values = att_val_map[ta_raw[i][0]] # returns list of tuples
            for j, word in enumerate(td_raw[i]):
                for val in possible_values:
                    pointer = 0
                    while (pointer < len(val) and j+pointer < len(td_raw[i])):
                        if td_raw[i][j+pointer] == val[pointer]:
                            pointer += 1
                        else:
                            break
                    if pointer == len(val):
                        prediction = list(val)   
        else: 
            start = position_start_shifted[i]
            if start < 0: start = 0
            end = position_end_shifted[i]
            if start < end:
                prediction = td_raw[i][start:end+1]
            else:
                prediction = [td_raw[i][start]]
            predictions[i] = prediction
            #print('ex num:', i)
            #print('pred:', prediction)
            #print("correct:", tv_raw[i])            
        if prediction == tv_raw[i]:
            correct_count_test += 1
            correct_indices_test += [i]
            if len(prediction) > 1:
                correct_count_mult_test += 1
        total_count_test += 1
        if len(tv_raw[i]) > 1:
            total_count_mult_test += 1        
    
    print('test accuracy in %:', float(correct_count_test)/total_count_test*100)
    print('test accuracy on multiple word values in %:', float(correct_count_mult_test)/total_count_mult_test*100)

