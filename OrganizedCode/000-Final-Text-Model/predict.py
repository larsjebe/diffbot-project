import data_parser

import numpy as np
import collections

from gensim.models import Word2Vec
from gensim.models import KeyedVectors

from keras.preprocessing import sequence
from keras.models import load_model

from  more_itertools import unique_everseen

BASE_DIR = '../011-Common-Files/'
EMBEDDING_FILE = BASE_DIR + 'GoogleNews-vectors-negative300.bin'
TRAIN_DATA_FILE = BASE_DIR + 'trainExamples.json'
TEST_DATA_FILE = BASE_DIR + 'devExamples.json'

TRAIN_ERROR = True
TEST_ERROR = True

TOY_DATASET = True
NUMBER_FILES = 150   #150 for complete train/test error. takes about 1hr. Complete Dataset needed (not submitted)
MAX_SEQUENCE_LENGTH = 160
MAX_VALUE_LENGTH = 10

MODEL_NAME = '72percentModel'


# load word index from google vocab
word_index = np.load('word_index_'+MODEL_NAME+'.npy').item() 
print ('Length of used vocab: %d' % (len(word_index)))

#load attribute index
att_index = np.load('att_index.npy').item() 
att_index_len = len(att_index)

#load attribute-value-map index
att_val_map = np.load('att_val_count_map.npy').item() # its a dict of tuples (key is string)

# index all possible tuples in 0 to (MAX_SEQUENCE_LENGTH-1) that are MAX_VALUE_LENGTH-1 or less apart
tuple_dict = {}
index = 0
for i in range(MAX_SEQUENCE_LENGTH):
    for j in range(MAX_VALUE_LENGTH):
        if i+j < MAX_SEQUENCE_LENGTH:
            tuple_dict[(i,i+j)] = index
            index += 1
            
#reverse tuple_dict
inv_tuple_dict = {v: k for k, v in tuple_dict.items()}


# get training data
if TOY_DATASET:
    if TRAIN_ERROR:
        d_raw, a_raw, v_raw = data_parser.get_dav_triples(TRAIN_DATA_FILE, MAX_LENGTH=MAX_SEQUENCE_LENGTH, CUSTOM_ATTS = True)
    if TEST_ERROR:
        # try for predictable only data
        td_raw, ta_raw, tv_raw = data_parser.get_dav_triples(TEST_DATA_FILE, MAX_LENGTH=MAX_SEQUENCE_LENGTH, CUSTOM_ATTS = True)
        # try for all data, not predictable only
        #td_raw, ta_raw, tv_raw = data_parser.get_complete_dav_triples(TEST_DATA_FILE, MAX_LENGTH=MAX_SEQUENCE_LENGTH, CUSTOM_ATTS = True)

else:
    if TRAIN_ERROR:
        d_raw, a_raw, v_raw = \
        data_parser.get_train_dav_triples( \
        NUM_FILES = NUMBER_FILES, MAX_LENGTH = MAX_SEQUENCE_LENGTH, CUSTOM_ATTS = True)
    if TEST_ERROR:
        td_raw, ta_raw, tv_raw = \
        data_parser.get_test_dav_triples( \
        NUM_FILES = NUMBER_FILES, MAX_LENGTH = MAX_SEQUENCE_LENGTH, CUSTOM_ATTS = True) 
        
        
if TRAIN_ERROR:
    # vectorize words
    d = [[word_index[word] for word in desc] for desc in d_raw]
    a = [att_index[word[0]] for word in a_raw]
    num_examples = len(a)
    

    #shift position by padding (one number per example)
    train_shifts = [MAX_SEQUENCE_LENGTH-len(d_raw[ex_idx]) for ex_idx, _ in enumerate(v_raw)]

    
    # create recommendation vectors from data and att_val_map
    rec_val = np.zeros((num_examples, len(tuple_dict)))

    for i in range(num_examples):
        best_fit = 0
        start = -1
        if a_raw[i][0] in att_val_map:
            possible_values = att_val_map[a_raw[i][0]] # returns dict of value tuple --> count
            for j, word in enumerate(d_raw[i]):
                for val, count in possible_values.items():
                    pointer = 0
                    while (pointer < len(val) and j+pointer < len(d_raw[i])):
                        if d_raw[i][j+pointer] == val[pointer]:
                            pointer += 1
                        else:
                            break
                    if pointer == len(val) and len(val) > 0:
                        if best_fit < count:
                            best_fit = count
                            end = j + pointer-1 + train_shifts[i]
                            start = j + train_shifts[i]
        # for loop
        if start >= 0:
            rec_val[i][tuple_dict[(start, end)]] = 1
            
    # create gournd truth tuples in one-hot based on the tuple_dict
    v_tuples = np.zeros((num_examples, len(tuple_dict)))
    for i in range(num_examples):
        start = -1
        for j, word in enumerate(d_raw[i]):
            pointer = 0
            while (pointer < len(v_raw[i]) and j+pointer < len(d_raw[i])):
                if d_raw[i][j+pointer] == v_raw[i][pointer]:
                    pointer += 1
                else:
                    break
            if pointer == len(v_raw[i]) and len(v_raw[i]) > 0:
                end = j + pointer-1 + train_shifts[i]
                start = j + train_shifts[i]
                v_tuples[i][tuple_dict[(start, end)]] = 1
                break

    # pad input data
    d = sequence.pad_sequences(d, maxlen=MAX_SEQUENCE_LENGTH)
    a = np.array(a)
    
    
if TEST_ERROR:
    # vectorize words
    td = [[word_index[word] for word in desc] for desc in td_raw]
    ta = [att_index[word[0]] for word in ta_raw]
    num_test_examples = len(ta)
    

    #shift position by padding (one number per example)
    test_shifts = [MAX_SEQUENCE_LENGTH-len(td_raw[ex_idx]) for ex_idx, _ in enumerate(tv_raw)]

    
    # create recommendation vectors from data and att_val_map
    trec_val = np.zeros((num_test_examples, len(tuple_dict)))

    for i in range(num_test_examples):
        best_fit = 0
        start = -1
        if ta_raw[i][0] in att_val_map:
            possible_values = att_val_map[ta_raw[i][0]] # returns dict of value tuple --> count
            for j, word in enumerate(td_raw[i]):
                for val, count in possible_values.items():
                    pointer = 0
                    while (pointer < len(val) and j+pointer < len(td_raw[i])):
                        if td_raw[i][j+pointer] == val[pointer]:
                            pointer += 1
                        else:
                            break
                    if pointer == len(val) and len(val) > 0:
                        if best_fit < count:
                            best_fit = count
                            end = j + pointer-1 + test_shifts[i]
                            start = j + test_shifts[i]
        # for loop
        if start >= 0:
            trec_val[i][tuple_dict[(start, end)]] = 1
            
    # create gournd truth tuples in one-hot based on the tuple_dict
    tv_tuples = np.zeros((num_test_examples, len(tuple_dict)))
    for i in range(num_test_examples):
        start = -1
        for j, word in enumerate(td_raw[i]):
            pointer = 0
            while (pointer < len(tv_raw[i]) and j+pointer < len(td_raw[i])):
                if td_raw[i][j+pointer] == tv_raw[i][pointer]:
                    pointer += 1
                else:
                    break
            if pointer == len(tv_raw[i]) and len(tv_raw[i]) > 0:
                end = j + pointer-1 + test_shifts[i]
                start = j + test_shifts[i]
                tv_tuples[i][tuple_dict[(start, end)]] = 1
                break

    # pad input data
    td = sequence.pad_sequences(td, maxlen=MAX_SEQUENCE_LENGTH)
    ta = np.array(ta)
    
    
model = load_model(MODEL_NAME + '.h5')


if TRAIN_ERROR:
    total_count_train = 0
    correct_count_train = 0
    correct_indices_train = []
    correct_count_mult_train = 0
    total_count_mult_train = 0
    sm = model.predict([np.squeeze(d[:,None]), np.squeeze(a[:,None]), np.squeeze(rec_val[:,None])])
    tuple_indices = [sm[i][:].argmax() for i in range(np.shape(sm)[0])]
    
    num_examples = len(tuple_indices)
    
    predictions = {}
    
    for i in range(num_examples):
        start = inv_tuple_dict[tuple_indices[i]][0]-train_shifts[i]
        end = inv_tuple_dict[tuple_indices[i]][1]-train_shifts[i]
        if start >= 0:
            if end+1 < len(d_raw[i]):
                prediction = d_raw[i][start:end+1]
            else:
                prediction = d_raw[i][0]
            predictions[i] = prediction
            #print('ex num:', i)
            #print('pred:', prediction)
            #print("correct:", v_raw[i])
            if prediction == v_raw[i]:
                correct_count_train += 1
                correct_indices_train += [i]
                if len(prediction) > 1:
                    correct_count_mult_train += 1
        total_count_train += 1
        if len(v_raw[i]) > 1:
            total_count_mult_train += 1        
    
    print('train accuracy in %:', float(correct_count_train)/total_count_train*100)
    print('train accuracy on multiple word values in %:', float(correct_count_mult_train)/total_count_mult_train*100)
    
if TEST_ERROR:
    total_count_test = 0
    correct_count_test = 0
    correct_indices_test = []
    correct_count_mult_test = 0
    total_count_mult_test = 0
    tsm = model.predict([np.squeeze(td[:,None]), np.squeeze(ta[:,None]), np.squeeze(trec_val[:,None])])
    tuple_indices = [tsm[i][:].argmax() for i in range(np.shape(tsm)[0])]
    not_in_padding = 0
    
    num_test_examples = len(tuple_indices)
    
    tpredictions = {}
    
    for i in range(num_test_examples):
        start = inv_tuple_dict[tuple_indices[i]][0]-test_shifts[i]
        end = inv_tuple_dict[tuple_indices[i]][1]-test_shifts[i]
        if start >= 0:
            not_in_padding += 1
            if end+1 < len(td_raw[i]):
                prediction = td_raw[i][start:end+1]
            else:
                prediction = td_raw[i][0]
            tpredictions[i] = prediction
            #print('ex num:', i)
            #print('pred:', prediction)
            #print("correct:", tv_raw[i])
            if prediction == tv_raw[i]:
                correct_count_test += 1
                correct_indices_test += [i]
                if len(prediction) > 1:
                    correct_count_mult_test += 1
        total_count_test += 1
        if len(tv_raw[i]) > 1:
            total_count_mult_test += 1        
    
    print('test accuracy in %:', float(correct_count_test)/total_count_test*100)
    print('test accuracy on multiple word values in %:', float(correct_count_mult_test)/total_count_mult_test*100)
    print('% predictions not in padding:', float(not_in_padding)/total_count_test*100)

