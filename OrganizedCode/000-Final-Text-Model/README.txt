########### 000-Final-Text-Model ###########

#### train.py ####

- Specifies Parameters
- Loads specified amount of training data
- Trains model
- Saves trained model for future use
- Saves index-files created for training for future use
- NOTE: Only runs when Google's Word Embedding (Size ~3GB) is downloaded and present in the path specified in train.py!


#### predict.py ####

- Loads a model by name
- Loads specified train- and test data
- Predicts on train and test data and prints accuracy measures


#### data_parser.py ####

- contains different functions for data parsing
- called by train.py and predict.py to load data from file


#### 72percentModel.h5 ####

- trained model that achieves ~72% accuracy


#### att_index.npy ####

- Attribute Index used for Custom Attribute Embedding


#### att_val_count_map.npy ####

- Value Categories used to construct recommendations
- Refer to data_parser.py to see how this is constructed


#### word_index_72percentModel.npy ####

- Vocabulary index for word embedding
