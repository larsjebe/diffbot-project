# README


# Codebase: Multi-Modal Information Extraction in a Question-Answer Framework

MuMIE Project, Fall 2017, by Vamsi Chitters, Fabian Frank, Lars Jebe
-----------------------------------

Codebase contains the following 4 folders: 

- 000-Final-Text-Model
- 001-Final-Text-and-Image-Model
- 002-Other-Experiments
- 011-Common-Files

## 000-Final-Text-Model 

#### train.py 

- Specifies Parameters
- Loads specified amount of training data
- Trains model
- Saves trained model for future use
- Saves index-files created for training for future use
- NOTE: Only runs when Google's Word Embedding (Size ~3GB) is downloaded and present in the path specified in train.py!


#### predict.py 

- Loads a model by name
- Loads specified train- and test data
- Predicts on train and test data and prints accuracy measures


#### data_parser.py 

- contains different functions for data parsing
- called by train.py and predict.py to load data from file


#### 72percentModel.h5 

- trained model that achieves ~72% accuracy


#### att_index.npy 

- Attribute Index used for Custom Attribute Embedding


#### att_val_count_map.npy 

- Value Categories used to construct recommendations
- Refer to data_parser.py to see how this is constructed


#### word_index_72percentModel.npy 

- Vocabulary index for word embedding

## 001-Final-Text-and-Image-Model

Contains the model for images and text combined. 


## 002-Other-Experiments 

Contains Code for other model architectures and parameters that have been tested. 


## 011-Common-Files 

Folder that contains files (index files, subset of training data, google word embedding matrix) that are shared across all experiments. Some of these files have been included in folder 000-Final-Text-Model as well for convenience. 


## NOTE 

Other experiment folders are structured in a similar way. 


## Commands to run the code:

From within an experiment folder, use this command to run the code on the small subset of data provided with this release:
>>> python train.py

From within an experiment folder, if there is a saved experiment present (either because it's provided or because train.py has been run)
- in predict.py: change MODEL_NAME to the name of the saved model that shall be used for predictions and execute: 
>>> python predict.py
to generate train/test error print
