from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import collections
import math
import os
import random
import zipfile
import json

import numpy as np
from six.moves import urllib
from six.moves import xrange  
import tensorflow as tf


BASE_DIR = '../011-Common-Files/'

# Step 1: Fetch the data.
trainingFilename = 'trainExamples.json'
testFilename = 'devExamples.json'

# Read the data into a list of strings.
def read_training_data(filename):
  json_data=open(filename)
  data = json.load(json_data)
  json_data.close()

  fullText = ""
  for element in data:
    title = element["title"]
    text = element["text"]
    fullText += title + " " + text + " "
    for attribute in element["specs"]:
        value = element["specs"][attribute]
        fullText += attribute.replace("_", " ") + " " + value + " "
  lower_fulltext = fullText.lower()
  data = tf.compat.as_str(lower_fulltext).split()
  return data

def read_test_data(filename):
  json_data=open(filename)
  data = json.load(json_data)
  json_data.close()

  testKVPairs = []
  testTokens = {}
  for elementIdx, element in enumerate(data):
    tokenList = []
    AVMapping = {}
    for token in element["tokens"]:
        tokenList.append(tf.compat.as_str(token).lower())
    testTokens[elementIdx] = tokenList
    for attribute in element["specs"]:
        value = element["specs"][attribute]
        key = tf.compat.as_str(attribute).replace("_", " ").lower()
        AVMapping[key] = tf.compat.as_str(value).lower()
    testKVPairs.append((AVMapping, elementIdx))

  return testKVPairs, testTokens

vocabulary = read_training_data(trainingFilename)
print('Data size', len(vocabulary))

testKVPairs, testTokens = read_test_data(testFilename)
trainKVPairs, trainTokens = read_test_data(trainingFilename)

# Step 2: Build the dictionary and replace rare words with UNK token.
vocabulary_size = 10000

def build_dataset(words, n_words):
  """Process raw inputs into a dataset."""
  count = [['UNK', -1]]
  count.extend(collections.Counter(words).most_common(n_words - 1))
  dictionary = dict()
  for word, _ in count:
    dictionary[word] = len(dictionary)
  data = list()
  unk_count = 0
  for word in words:
    if word in dictionary:
      index = dictionary[word]
    else:
      index = 0  # dictionary['UNK']
      unk_count += 1
    data.append(index)
  count[0][1] = unk_count
  reversed_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
  return data, count, dictionary, reversed_dictionary

data, count, dictionary, reverse_dictionary = build_dataset(vocabulary,
                                                            vocabulary_size)
del vocabulary  # Hint to reduce memory.
print('Most common words (+UNK)', count[:5])
print('Sample data', data[:10], [reverse_dictionary[i] for i in data[:10]])

data_index = 0

# Step 3: Function to generate a training batch for the skip-gram model.
def generate_batch(batch_size, num_skips, skip_window):
  global data_index
  assert batch_size % num_skips == 0
  assert num_skips <= 2 * skip_window
  batch = np.ndarray(shape=(batch_size), dtype=np.int32)
  labels = np.ndarray(shape=(batch_size, 1), dtype=np.int32)
  span = 2 * skip_window + 1  # [ skip_window target skip_window ]
  buffer = collections.deque(maxlen=span)
  if data_index + span > len(data):
    data_index = 0
  buffer.extend(data[data_index:data_index + span])
  data_index += span
  for i in range(batch_size // num_skips):
    target = skip_window  # target label at the center of the buffer
    targets_to_avoid = [skip_window]
    for j in range(num_skips):
      while target in targets_to_avoid:
        target = random.randint(0, span - 1)
      targets_to_avoid.append(target)
      batch[i * num_skips + j] = buffer[skip_window]
      labels[i * num_skips + j, 0] = buffer[target]
    if data_index == len(data):
      for word in data[:span]:
        buffer.append(word)
      data_index = span
    else:
      buffer.append(data[data_index])
      data_index += 1
  # Backtrack a little bit to avoid skipping words in the end of a batch
  data_index = (data_index + len(data) - span) % len(data)
  return batch, labels

batch, labels = generate_batch(batch_size=8, num_skips=2, skip_window=1)
for i in range(8):
  print(batch[i], reverse_dictionary[batch[i]],
        '->', labels[i, 0], reverse_dictionary[labels[i, 0]])

# Step 4: Build and train a skip-gram model.

batch_size = 128
embedding_size = 128  # Dimension of the embedding vector.
skip_window = 1       # How many words to consider left and right.
num_skips = 2         # How many times to reuse an input to generate a label.

# We pick a random validation set to sample nearest neighbors. Here we limit the
# validation samples to the words that have a low numeric ID, which by
# construction are also the most frequent.
valid_size = 16     # Random set of words to evaluate similarity on.
valid_window = 100  # Only pick dev samples in the head of the distribution.
#valid_examples = np.random.choice(valid_window, valid_size, replace=False)
valid_examples = [] 
valid_tokens = []
attribute_list = []
for AVMap, elementIdx in testKVPairs:
    for attribute in AVMap:
        attribute_list.append((attribute, elementIdx)) 
for AVMap, elementIdx in testKVPairs:
    for attribute in AVMap:
        if attribute in dictionary:
            valid_examples.append(dictionary[attribute])
            valid_tokens.append(elementIdx)
print('%d test attributes found in the dictionary' % (len(valid_examples)))
            
# do the same for training set to be able to check training error
train_examples = []
train_tokens = []
train_att_list = []
for AVMap, elementIdx in trainKVPairs:
    for attribute in AVMap:
        train_att_list.append((attribute, elementIdx)) 
for AVMap, elementIdx in trainKVPairs:
    for attribute in AVMap:
        if attribute in dictionary:
            train_examples.append(dictionary[attribute])
            train_tokens.append(elementIdx)
print('%d train attributes found in the dictionary' % (len(train_examples)))



num_sampled = 64    # Number of negative examples to sample.

graph = tf.Graph()

with graph.as_default():

  # Input data.
  train_inputs = tf.placeholder(tf.int32, shape=[batch_size])
  train_labels = tf.placeholder(tf.int32, shape=[batch_size, 1])
  valid_dataset = tf.constant(valid_examples, dtype=tf.int32)
  train_dataset = tf.constant(train_examples, dtype=tf.int32)

  # Ops and variables pinned to the CPU because of missing GPU implementation
  with tf.device('/cpu:0'):
    # Look up embeddings for inputs.
    embeddings = tf.Variable(
        tf.random_uniform([vocabulary_size, embedding_size], -1.0, 1.0))
    embed = tf.nn.embedding_lookup(embeddings, train_inputs)

    # Construct the variables for the NCE loss
    nce_weights = tf.Variable(
        tf.truncated_normal([vocabulary_size, embedding_size],
                            stddev=1.0 / math.sqrt(embedding_size)))
    nce_biases = tf.Variable(tf.zeros([vocabulary_size]))

  # Compute the average NCE loss for the batch.
  # tf.nce_loss automatically draws a new sample of the negative labels each
  # time we evaluate the loss.
  loss = tf.reduce_mean(
      tf.nn.nce_loss(weights=nce_weights,
                     biases=nce_biases,
                     labels=train_labels,
                     inputs=embed,
                     num_sampled=num_sampled,
                     num_classes=vocabulary_size))

  # Construct the SGD optimizer using a learning rate of 1.0.
  optimizer = tf.train.GradientDescentOptimizer(1.0).minimize(loss)

  # Compute the cosine similarity between minibatch examples and all embeddings.
  norm = tf.sqrt(tf.reduce_sum(tf.square(embeddings), 1, keep_dims=True))
  normalized_embeddings = embeddings / norm
  valid_embeddings = tf.nn.embedding_lookup(
      normalized_embeddings, valid_dataset)
  train_embeddings = tf.nn.embedding_lookup(
      normalized_embeddings, train_dataset)
  similarity = tf.matmul(
      valid_embeddings, normalized_embeddings, transpose_b=True)
  similarityTrain = tf.matmul(
      train_embeddings, normalized_embeddings, transpose_b=True)

  # Add variable initializer.
  init = tf.global_variables_initializer()

# Step 5: Begin training.
num_steps = 100001

with tf.Session(graph=graph) as session:
  # We must initialize all variables before we use them.
  init.run()
  print('Initialized')

  average_loss = 0
  for step in xrange(num_steps):
    batch_inputs, batch_labels = generate_batch(
        batch_size, num_skips, skip_window)
    feed_dict = {train_inputs: batch_inputs, train_labels: batch_labels}

    # We perform one update step by evaluating the optimizer op (including it
    # in the list of returned values for session.run()
    _, loss_val = session.run([optimizer, loss], feed_dict=feed_dict)
    average_loss += loss_val

    if step % 2000 == 0:
      if step > 0:
        average_loss /= 2000
      # The average loss is an estimate of the loss over the last 2000 batches.
      print('Average loss at step ', step, ': ', average_loss)
      average_loss = 0

    # Note that this is expensive (~20% slowdown if computed every 500 steps)
    if step % 20000 == 0:
      sim = similarity.eval()
      for i in xrange(valid_size):
        prediction_list = []
        valid_word = reverse_dictionary[valid_examples[i]]
        top_k = 8  # number of nearest neighbors
        nearest = (-sim[i, :]).argsort()[1:top_k + 1]
        nearest_all = (-sim[i, :]).argsort()[1:]
        for nearest_idx in nearest_all:
            if reverse_dictionary[nearest_idx] in testTokens[valid_tokens[i]]:
                prediction_list.append(reverse_dictionary[nearest_idx])
            if len(prediction_list) >= 10:
                break
        print('Nearest to:', valid_word, ':', prediction_list)
        """log_str = 'Nearest to %s:' % valid_word
        for k in xrange(top_k):
          close_word = reverse_dictionary[nearest[k]]
          log_str = '%s %s,' % (log_str, close_word)
        print(log_str)"""
  final_embeddings = normalized_embeddings.eval()

  # evaluate performance on test set
  sim = similarity.eval()
    
  acc1_count = 0
  acc10_count = 0
  trial_count = 0
  for i in xrange(len(valid_examples)):
    prediction_list = []
    valid_word = reverse_dictionary[valid_examples[i]]
    nearest_all = (-sim[i, :]).argsort()[1:]
    for nearest_idx in nearest_all:
        if reverse_dictionary[nearest_idx] in testTokens[valid_tokens[i]]:
            trial_count += 1
            predicted_value = reverse_dictionary[nearest_idx]
            AVMap, _ = testKVPairs[valid_tokens[i]]
            if predicted_value == AVMap[valid_word]:
                acc10_count += 1
                if trial_count <= 1:
                    acc1_count += 1
                print('Correct Test Prediction: Attribute:', valid_word, 'Value:', AVMap[valid_word], 'on example number:', valid_tokens[i])
                break
            elif trial_count >= 10:
                trial_count = 0
                break

  print('Test Acc@10 for valid in %%:', 100*float(acc10_count)/len(valid_examples))
  print('Test Acc@10 total in %%:', 100*float(acc10_count)/len(attribute_list))
  print('Test Acc@1 for valid in %%:', 100*float(acc1_count)/len(valid_examples))
  print('Test Acc@1 total in %%:', 100*float(acc1_count)/len(attribute_list))
    
# evaluate performance on training
  simTrain = similarityTrain.eval()
    
  acc1_count = 0
  acc10_count = 0
  trial_count = 0
  for i in xrange(len(train_examples)):
    prediction_list = []
    valid_word = reverse_dictionary[train_examples[i]]
    nearest_all = (-simTrain[i, :]).argsort()[1:]
    for nearest_idx in nearest_all:
        if reverse_dictionary[nearest_idx] in trainTokens[train_tokens[i]]:
            trial_count += 1
            predicted_value = reverse_dictionary[nearest_idx]
            AVMap, _ = trainKVPairs[train_tokens[i]]
            if predicted_value == AVMap[valid_word]:
                acc10_count += 1
                if trial_count <= 1:
                    acc1_count += 1
                print('Correct Train Prediction: Attribute:', valid_word, 'Value:', AVMap[valid_word], 'on example number:', train_tokens[i])
                break
            elif trial_count >= 10:
                trial_count = 0
                break

  print('Train Acc@10 for valid in %%:', 100*float(acc10_count)/len(valid_examples))
  print('Train Acc@10 total in %%:', 100*float(acc10_count)/len(attribute_list))
  print('Train Acc@1 for valid in %%:', 100*float(acc1_count)/len(valid_examples))
  print('Train Acc@1 total in %%:', 100*float(acc1_count)/len(attribute_list))
    
# Step 6: Visualize the embeddings.


def plot_with_labels(low_dim_embs, labels, filename='tsne.png'):
  assert low_dim_embs.shape[0] >= len(labels), 'More labels than embeddings'
  plt.figure(figsize=(18, 18))  # in inches
  for i, label in enumerate(labels):
    x, y = low_dim_embs[i, :]
    plt.scatter(x, y)
    plt.annotate(label,
                 xy=(x, y),
                 xytext=(5, 2),
                 textcoords='offset points',
                 ha='right',
                 va='bottom')

  plt.savefig(filename)

try:
  # pylint: disable=g-import-not-at-top
  from sklearn.manifold import TSNE
  import matplotlib.pyplot as plt

  tsne = TSNE(perplexity=30, n_components=2, init='pca', n_iter=5000, method='exact')
  plot_only = 500
  low_dim_embs = tsne.fit_transform(final_embeddings[:plot_only, :])
  labels = [reverse_dictionary[i] for i in xrange(plot_only)]
  plot_with_labels(low_dim_embs, labels)

except ImportError:
  print('Please install sklearn, matplotlib, and scipy to show embeddings.')
