import data_parser

import numpy as np
np.random.seed(1337)
import collections

from gensim.models import Word2Vec
from gensim.models import KeyedVectors

from keras.layers import Embedding
from keras.models import Sequential
from keras.layers import Dropout
from keras.layers import Dense
from keras.layers import LSTM
from keras.preprocessing import sequence
from keras.models import load_model

from  more_itertools import unique_everseen

BASE_DIR = '../011-Common-Files/'
EMBEDDING_FILE = BASE_DIR + 'GoogleNews-vectors-negative300.bin'
TRAIN_DATA_FILE = BASE_DIR + 'trainExamples.json'
TEST_DATA_FILE = BASE_DIR + 'devExamples.json'


# get training data
X_train_raw, y_train_raw = data_parser.get_data(TRAIN_DATA_FILE, MAX_LENGTH = 150)

# get test data
X_test_raw, y_test_raw = data_parser.get_data(TEST_DATA_FILE, MAX_LENGTH = 150)

MAX_SEQUENCE_LENGTH = max([len(X) for X in X_train_raw])
MAX_OUTPUT_LENGTH = max([len(y) for y in y_train_raw+y_test_raw])

# create vocab from data
vocab_data = [j for i in X_train_raw+X_test_raw+y_train_raw+y_test_raw for j in i]
vocab_data = list(unique_everseen(vocab_data))

# load model
model = KeyedVectors.load_word2vec_format(EMBEDDING_FILE, binary=True)
vocab_goog = list(model.vocab.keys())

#vocab_used = list(set(vocab_data) & set(vocab_goog))
set_goog = frozenset(vocab_goog)
vocab_used = [x for x in vocab_data if x in set_goog]
print ('Length of used vocab: %d' % (len(vocab_used)))
vocab_length = len(vocab_used)

#index vocab
word_index = collections.defaultdict(int)
for i,word in enumerate(vocab_used):
    word_index[word] = i+1 # 0 reserved for Unk
    
# save the word index for later use
np.save('word_index.npy', word_index) 
    
# replace word data with index data
X_train = [[word_index[word] for word in description] for description in X_train_raw]
X_test = [[word_index[word] for word in description] for description in X_test_raw]
y_train_i = [[word_index[word] for word in description] for description in y_train_raw]
y_test_i = [[word_index[word] for word in description] for description in y_test_raw]

# convert y_train_i to numpy array
y_train_i = np.array(y_train_i)
y_test_i = np.array(y_test_i)

# get position of value in text (without padding)
y_train_idx = [[X_train_raw[ex_idx].index(word)+1 if (word in X_train_raw[ex_idx]) else 0 for word in value] for ex_idx, value in enumerate(y_train_raw)]

y_test_idx = [[X_test_raw[ex_idx].index(word)+1 if (word in X_test_raw[ex_idx]) else 0 for word in value] for ex_idx, value in enumerate(y_test_raw)]

#shift position by padding (one number per example)
train_shifts = [MAX_SEQUENCE_LENGTH-len(X_train_raw[ex_idx]) for ex_idx, value in enumerate(y_train_raw)]
y_train_idx_shifted = [[el + train_shifts[ex_idx] for el in pos_list] for ex_idx, pos_list in enumerate(y_train_idx)]

test_shifts = [MAX_SEQUENCE_LENGTH-len(X_test_raw[ex_idx]) for ex_idx, value in enumerate(y_test_raw)]
y_test_idx_shifted = [[el + test_shifts[ex_idx] for el in pos_list] for ex_idx, pos_list in enumerate(y_test_idx)]

# convert y_train_idx_shifted to one-hot (or two-hot etc.)
y_train = np.zeros((len(y_train_i), MAX_SEQUENCE_LENGTH+1)) 
indices = [(i,word_idx) for i,pos in enumerate(y_train_idx_shifted) for word_idx in pos]
for a,b in indices:
    y_train[a][b] = 1 
    
y_test = np.zeros((len(y_test_i), MAX_SEQUENCE_LENGTH+1))
indices = [(i,word_idx) for i,pos in enumerate(y_test_idx_shifted) for word_idx in pos]
for a,b in indices:
    y_test[a][b] = 1 
        

# pad input data
X_train = sequence.pad_sequences(X_train, maxlen=MAX_SEQUENCE_LENGTH)
X_test = sequence.pad_sequences(X_test, maxlen=MAX_SEQUENCE_LENGTH)



#### CREATE MODEL ####
# create embedding matrix
EMBEDDING_DIM = len(model['this']) # length of word vectors in google-embedding
    
embedding_matrix = np.zeros((vocab_length + 1, EMBEDDING_DIM))


for word, idx in word_index.items():
    if word not in model: # TODO: why does this happen
        continue
    embedding_matrix[idx] = model[word]
    
# create keras embedding layer
embedding_layer = Embedding(vocab_length + 1,
                            EMBEDDING_DIM,
                            weights=[embedding_matrix],
                            input_length=MAX_SEQUENCE_LENGTH,
                            trainable=False)


model = Sequential()
model.add(embedding_layer)
model.add(LSTM(MAX_SEQUENCE_LENGTH))
#model.add(Dropout(0.2))
model.add(Dense(MAX_SEQUENCE_LENGTH+1, activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
print(model.summary())

#### TRAIN MODEL ####

print('Model is training....')
model.fit(X_train, y_train, epochs=5, batch_size=16)
print('Training finished!')

print('Saving Model...')
model.save('FifthModelTrained.h5')

print('Evaluating Model...')
scores = model.evaluate(np.squeeze(X_test[:,None]), y_test)

print ('scores:', scores)   