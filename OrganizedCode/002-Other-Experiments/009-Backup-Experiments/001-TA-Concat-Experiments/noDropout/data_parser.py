import os
import re
import json
import glob

from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer

STOP = True
STEM = False


MOD_FILE_NUM = 5

TRAIN_DIRECTORY = '../../training_data/train/'


## Json Parser
# Read the data into a list of strings.
def read_training_data(filename):
    json_data=open(filename)
    data = json.load(json_data)
    json_data.close()
    
    sentences = []

    for element in data:
        split_title = element["title"].split(". ")
        split_text = element["text"].split(". ")
        for item in split_title:   
            sentences.append(item)
        for item in split_text:
            sentences.append(item) # append all sentences in the despcription
  

    # returns a list of strings, each string is a complete sentence
    return sentences


# The function "text_to_wordlist" is from
# https://www.kaggle.com/currie32/quora-question-pairs/the-importance-of-cleaning-text
def sentence_to_wordlist(text, remove_stopwords=False, stem_words=False, complete_sentence=False):
    # Clean the sentence, with the option to remove stopwords and to stem words.
    
    # Convert words to lower case and split them
    text = text.lower().split()

    # Optionally, remove stop words
    if remove_stopwords:
        stops = set(stopwords.words("english"))
        text = [w for w in text if not w in stops]
    
    text = " ".join(text)

    # Clean the text
    
    text = re.sub(r"[^A-Za-z0-9^,!\/'+-=]", " ", text)
    text = re.sub(r"\. ", " ", text)
    text = re.sub(r"what's", "what is ", text)
    text = re.sub(r"\'s", " ", text)
    text = re.sub(r"\'ve", " have ", text)
    text = re.sub(r"can't", "cannot ", text)
    text = re.sub(r"n't", " not ", text)
    text = re.sub(r"i'm", "i am ", text)
    text = re.sub(r"\'re", " are ", text)
    text = re.sub(r"\'d", " would ", text)
    text = re.sub(r"\'ll", " will ", text)
    text = re.sub(r",", " ", text)
    #text = re.sub(r"\.", " ", text)
    text = re.sub(r"!", " ! ", text)
    text = re.sub(r"\/", " ", text)
    text = re.sub(r"\^", " ^ ", text)
    text = re.sub(r"\+", " + ", text)
    text = re.sub(r"\-", " - ", text)
    text = re.sub(r"\=", " = ", text)
    text = re.sub(r"'", " ", text)
    text = re.sub(r"(\d+)(k)", r"\g<1>000", text)
    text = re.sub(r":", " : ", text)
    text = re.sub(r" e g ", " eg ", text)
    text = re.sub(r" b g ", " bg ", text)
    text = re.sub(r" u s ", " american ", text)
    text = re.sub(r"\0s", "0", text)
    text = re.sub(r" 9 11 ", "911", text)
    text = re.sub(r"e - mail", "email", text)
    text = re.sub(r"j k", "jk", text)
    text = re.sub(r"\s{2,}", " ", text)
   
    
    # Optionally, shorten words to their stems
    if stem_words:
        text = text.split()
        stemmer = SnowballStemmer('english')
        stemmed_words = [stemmer.stem(word) for word in text]
        text = " ".join(stemmed_words)
    
    # Return a list of words
    if complete_sentence:
        return (text)
    else:
        return(text.split())   

# take in json file and return a list of lists of tokens in sentences
def parse_data(filename, complete_sentence=False):
    sentences = read_training_data(filename)

    token_lists = []
    for sen in sentences:
        token_lists.append(sentence_to_wordlist(sen,complete_sentence = complete_sentence))
        
    return token_lists

def get_data(filename, MAX_LENGTH = 150):
    
    json_data=open(filename)
    data = json.load(json_data)
    json_data.close()
    
    X_data = []
    y_data = []

    for element in data:
        title = element["title"]
        text = element["text"]

        for attribute in element["specs"]:
            value = sentence_to_wordlist(element["specs"][attribute], remove_stopwords=False, stem_words=STEM)
            raw_att = attribute.replace("_", " ")
            att = sentence_to_wordlist(raw_att, remove_stopwords=STOP, stem_words=STEM)
            joined_att = " ".join(att)
            fullText = title + " " + text + " " + joined_att
            fullText = sentence_to_wordlist(fullText, remove_stopwords=STOP, stem_words=STEM)
            if len(fullText) > MAX_LENGTH:
                fullText = fullText[:int(MAX_LENGTH/2)]+fullText[-int(MAX_LENGTH/2):]
            X_data.append(fullText)
            y_data.append(value)
            
    return (X_data, y_data) 

def get_predictable_data(filename, MAX_LENGTH = 150):
    
    json_data=open(filename)
    data = json.load(json_data)
    json_data.close()
    
    X_data = []
    y_data = []

    for element in data:
        title = element["title"]
        text = element["text"]

        for attribute in element["specs"]:
            value = sentence_to_wordlist(element["specs"][attribute], remove_stopwords=False, stem_words=STEM)
            raw_att = attribute.replace("_", " ")
            att = sentence_to_wordlist(raw_att, remove_stopwords=STOP, stem_words=STEM)
            joined_att = " ".join(att)
            fullText = title + " " + text + " " + joined_att
            fullText = sentence_to_wordlist(fullText, remove_stopwords=STOP, stem_words=STEM)
            if " ".join(value) in " ".join(fullText):
                if len(fullText) > MAX_LENGTH:
                    fullText = fullText[:int(MAX_LENGTH/2)]+fullText[-int(MAX_LENGTH/2):]
                X_data.append(fullText)
                y_data.append(value)
            
    return (X_data, y_data) 





def get_more_predictable_data(NUM_FILES = 5, MAX_LENGTH = 150):
    X_train_data = []
    y_train_data = []
    X_test_data = []
    y_test_data = []  

    index = 0

    for file in glob.glob(TRAIN_DIRECTORY+'*.json'):
        if (index >= NUM_FILES): break
        index += 1

        json_data=open(file)
        data = json.load(json_data)
        json_data.close()    

        for element in data:
            title = element["title"]
            text = element["text"]       

            for attribute in element["specs"]:
                value = sentence_to_wordlist(element["specs"][attribute], remove_stopwords=STOP, stem_words=STEM)
                raw_att = attribute.replace("_", " ")
                att = sentence_to_wordlist(raw_att, remove_stopwords=STOP, stem_words=STEM)
                joined_att = " ".join(att)
                fullText = title + " " + text + " " + joined_att
                fullText = sentence_to_wordlist(fullText, remove_stopwords=STOP, stem_words=STEM)
                if len(fullText) > MAX_LENGTH:
                    fullText = fullText[:int(MAX_LENGTH/2)]+fullText[-int(MAX_LENGTH/2):]
                if (index % MOD_FILE_NUM != 0):
                    if (" ".join(value) in " ".join(fullText)):
                        X_train_data.append(fullText)
                        y_train_data.append(value)
                else:
                    X_test_data.append(fullText)
                    y_test_data.append(value)
                        
    return (X_train_data, y_train_data, X_test_data, y_test_data)


def get_more_test_data(NUM_FILES = 5, MAX_LENGTH = 150, predictable = False):
    NUM_FILES = 5*NUM_FILES
    
    X_test_data = []
    y_test_data = []  

    index = 0

    for file in glob.glob(TRAIN_DIRECTORY+'*.json'):
        if (index >= NUM_FILES): break
        index += 1
        if (index % MOD_FILE_NUM != 0):
            continue

        json_data=open(file)
        data = json.load(json_data)
        json_data.close()    

        for element in data:
            title = element["title"]
            text = element["text"]       

            for attribute in element["specs"]:
                value = sentence_to_wordlist(element["specs"][attribute], remove_stopwords=STOP, stem_words=STEM)
                raw_att = attribute.replace("_", " ")
                att = sentence_to_wordlist(raw_att, remove_stopwords=STOP, stem_words=STEM)
                joined_att = " ".join(att)
                fullText = title + " " + text + " " + joined_att
                fullText = sentence_to_wordlist(fullText, remove_stopwords=STOP, stem_words=STEM)
                if len(fullText) > MAX_LENGTH:
                    fullText = fullText[:int(MAX_LENGTH/2)]+fullText[-int(MAX_LENGTH/2):]
                    
                
                if predictable:
                    if " ".join(value) in " ".join(fullText):
                        X_test_data.append(fullText)
                        y_test_data.append(value)
                else: 
                        X_test_data.append(fullText)
                        y_test_data.append(value)
                        
    return (X_test_data, y_test_data)

def get_top100_test_data(NUM_FILES = 5, MAX_LENGTH = 150, predictable = False):
    NUM_FILES = 5*NUM_FILES
    
    X_test_data = []
    y_test_data = []  

    index = 0
    
    top_100 = ['color', 'finish', 'type', 'transmission', 'quantity', 'product type', 'color family', 'size', 'assembly required', 'fuel', 'interior', 'warranty', 'format', 'features', 'form factor', 'fuel type', 'category', 'binding', 'exterior', 'package quantity', 'year', 'doors', 'shape', 'previous owners', 'ships', 'usage', 'price', 'measurements', 'assembly', 'width', 'technical', 'availability', 'model', 'platform', 'sold', 'engine', 'general style', 'packaged quantity', 'location', 'certifications listings', 'colorfinish', 'shipping', 'seats', 'productgroup', 'free shipping', 'x - ray', 'paperback', 'voltage', 'height', 'colors', 'construction', 'lending', 'http', 'drive type', 'status', 'batteries included', 'weight', 'operating system', 'length', 'frame material', 'bluetooth', 'seating', 'dimension', 'engine size', 'care', 'color finish', 'indooroutdoor', 'alloys', 'power source', 'space saver', 'pattern', 'wheelbase', 'tyre size spare', 'packaging', 'fuel tank capacity litres', 'interior color', 'luggage capacity seats', 'product group', 'age', 'hardcover', 'tyre size front', 'aspect ratio', 'cylinders', 'pages', 'tyre size rear', 'turning circle - kerb kerb', 'number discs', 'mileage', 'movement', 'automatic', 'exterior color', 'number pieces', 'usb', 'product line', 'included', 'flooring product type', 'department', 'drive wheels', 'producttypename', 'resources']

    for file in glob.glob(TRAIN_DIRECTORY+'*.json'):
        if (index >= NUM_FILES): break
        index += 1
        if (index % MOD_FILE_NUM != 0):
            continue

        json_data=open(file)
        data = json.load(json_data)
        json_data.close()    

        for element in data:
            title = element["title"]
            text = element["text"]       

            for attribute in element["specs"]:
                value = sentence_to_wordlist(element["specs"][attribute], remove_stopwords=STOP, stem_words=STEM)
                raw_att = attribute.replace("_", " ")
                att = sentence_to_wordlist(raw_att, remove_stopwords=STOP, stem_words=STEM)
                joined_att = " ".join(att)
                if joined_att not in top_100:
                    continue
                fullText = title + " " + text + " " + joined_att
                fullText = sentence_to_wordlist(fullText, remove_stopwords=STOP, stem_words=STEM)
                if len(fullText) > MAX_LENGTH:
                    fullText = fullText[:int(MAX_LENGTH/2)]+fullText[-int(MAX_LENGTH/2):]
                    
                if predictable:
                    if " ".join(value) in " ".join(fullText):
                        X_test_data.append(fullText)
                        y_test_data.append(value)
                else: 
                        X_test_data.append(fullText)
                        y_test_data.append(value)
                
                        
    return (X_test_data, y_test_data)
