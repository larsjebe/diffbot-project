import data_parser

import numpy as np
import collections

from gensim.models import Word2Vec
from gensim.models import KeyedVectors

from keras.preprocessing import sequence
from keras.models import load_model

from  more_itertools import unique_everseen

BASE_DIR = '../011-Common-Files/'
EMBEDDING_FILE = BASE_DIR + 'GoogleNews-vectors-negative300.bin'
TRAIN_DATA_FILE = BASE_DIR + 'trainExamples.json'
TEST_DATA_FILE = BASE_DIR + 'devExamples.json'

TRAIN_ERROR = True
TEST_ERROR = True

CREATE_NEW_WORD_INDEX = False

# Number of LSTM Memory Units (= sequence length)
seq_len = 150


# get training data
X_train_raw, y_train_raw = data_parser.get_predictable_data(TRAIN_DATA_FILE, MAX_LENGTH = seq_len)
# get test data
X_test_raw, y_test_raw = data_parser.get_data(TEST_DATA_FILE, MAX_LENGTH = seq_len)

#X_train_raw, y_train_raw, X_test_raw, y_test_raw = \
#    data_parser.get_more_predictable_data(NUM_FILES = 5, MAX_LENGTH = 150)

MAX_SEQUENCE_LENGTH = max([len(X) for X in X_train_raw])


if CREATE_NEW_WORD_INDEX:
    # create vocab from data
    #vocab_data = [j for i in X_train_raw+X_test_raw+y_train_raw+y_test_raw for j in i]
    vocab_data = [j for i in X_train_raw+X_test_raw+y_train_raw for j in i]
    vocab_data = list(unique_everseen(vocab_data))

    # load google Word2vec model
    model = KeyedVectors.load_word2vec_format(EMBEDDING_FILE, binary=True)
    vocab_goog = list(model.vocab.keys())

    #vocab_used = intersection of our vocab and google's vocab
    set_goog = frozenset(vocab_goog)
    vocab_used = [x for x in vocab_data if x in set_goog]
    print ('Length of used vocab: %d' % (len(vocab_used)))
    vocab_length = len(vocab_used)

    #create word index for each word in vocab
    word_index = collections.defaultdict(int)
    for i,word in enumerate(vocab_used):
        word_index[word] = i+1 # 0 reserved for Unk
else:
    word_index = np.load('word_index.npy').item() # load word index from file
    print ('Length of used vocab: %d' % (len(word_index)))
    
np.save('word_index.npy', word_index)

    
# replace words with word indices
X_train = [[word_index[word] for word in description] for description in X_train_raw]
X_test = [[word_index[word] for word in description] for description in X_test_raw]
y_train_i = [[word_index[word] for word in description] for description in y_train_raw]
y_test_i = [[word_index[word] for word in description] for description in y_test_raw]

# convert y_train_i to numpy array
y_train_i = np.array(y_train_i)
y_test_i = np.array(y_test_i)

# get position of value in text (without padding)
y_train_idx = [[X_train_raw[ex_idx].index(word)+1 if (word in X_train_raw[ex_idx]) else 0 for word in value] for ex_idx, value in enumerate(y_train_raw)]
y_test_idx = [[X_test_raw[ex_idx].index(word)+1 if (word in X_test_raw[ex_idx]) else 0 for word in value] for ex_idx, value in enumerate(y_test_raw)]

#shift position to account for padding
train_shifts = [MAX_SEQUENCE_LENGTH-len(X_train_raw[ex_idx]) for ex_idx, value in enumerate(y_train_raw)]
y_train_idx_shifted = [[el + train_shifts[ex_idx] for el in pos_list] for ex_idx, pos_list in enumerate(y_train_idx)]

test_shifts = [MAX_SEQUENCE_LENGTH-len(X_test_raw[ex_idx]) for ex_idx, value in enumerate(y_test_raw)]
y_test_idx_shifted = [[el + test_shifts[ex_idx] for el in pos_list] for ex_idx, pos_list in enumerate(y_test_idx)]

# convert y_train_idx_shifted to one-hot (or two-hot if value is longer than 1 word etc.)
y_train = np.zeros((len(y_train_i), MAX_SEQUENCE_LENGTH+1)) 
indices = [(i,word_idx) for i,pos in enumerate(y_train_idx_shifted) for word_idx in pos]
for a,b in indices:
    y_train[a][b] = 1 
    
y_test = np.zeros((len(y_test_i), MAX_SEQUENCE_LENGTH+1))
indices = [(i,word_idx) for i,pos in enumerate(y_test_idx_shifted) for word_idx in pos]
for a,b in indices:
    y_test[a][b] = 1 
        

# pad input data
X_train = sequence.pad_sequences(X_train, maxlen=MAX_SEQUENCE_LENGTH)
X_test = sequence.pad_sequences(X_test, maxlen=MAX_SEQUENCE_LENGTH)


model = load_model('Toy_30E_B16_pred.h5')

THRESH_DECAY = 0.2

if TRAIN_ERROR:
# predictions using matrices
    total_count_train = 0
    correct_count_train = 0
    correct_indices_train = []
    correct_count_mult_train = 0
    total_count_mult_train = 0
    softmax = model.predict(np.squeeze(X_train[:, None]))
    position = [softmax[i][1:].argmax() for i in range(np.shape(softmax)[0])]
    position_shifted = [softmax[i][1:].argmax()-train_shifts[i] for i in range(np.shape(softmax)[0])]
    for i in range(len(position)):
        pos_s = position_shifted[i]
        soft = softmax[i]        
        if pos_s >= 0:
            prediction = [X_train_raw[i][pos_s]]
            step = 1
            # append next word to prediction if softmax is confident enough            
            while (position[i]+step < MAX_SEQUENCE_LENGTH) and (soft[position[i]+step+1] > float(soft[position[i]+step-1+1])*THRESH_DECAY): 
                prediction.append(X_train_raw[i][pos_s+step])
                step += 1
             
            if prediction == y_train_raw[i]:
                correct_count_train += 1
                correct_indices_train += [i]
                if len(prediction) > 1:
                    correct_count_mult_train += 1
        total_count_train += 1
        if len(y_train_raw[i]) > 1:
            total_count_mult_train += 1
        
    
    print('train accuracy in %:', float(correct_count_train)/total_count_train*100)
    print('train accuracy on multiple word values in %:', float(correct_count_mult_train)/total_count_mult_train*100)

if TEST_ERROR:
    total_count_test = 0
    correct_count_test = 0
    correct_indices_test = []
    softmax = model.predict(np.squeeze(X_test[:, None]))
    position = [softmax[i][1:].argmax() for i in range(np.shape(softmax)[0])]
    position_shifted = [softmax[i][1:].argmax()-test_shifts[i] for i in range(np.shape(softmax)[0])]
    for i in range(len(position)):
        pos_s = position_shifted[i]
        soft = softmax[i]        
        if pos_s >= 0:
            prediction = [X_test_raw[i][pos_s]]
            step = 1
            # append next word to prediction if softmax is confident enough            
            while (position[i]+step < MAX_SEQUENCE_LENGTH) and (soft[position[i]+step+1] > float(soft[position[i]+step-1+1])*THRESH_DECAY): 
                prediction.append(X_test_raw[i][pos_s+step])
                step += 1
             
            if prediction == y_test_raw[i]:
                correct_count_test += 1
                correct_indices_test += [i]
        total_count_test += 1

    print('test accuracy in %:', float(correct_count_test)/total_count_test*100)

'''
# predict on whole train dataset (only one word values for now)
total_count_train = 0
correct_count_train = 0
correct_indices_train = []
for i in range(len(X_train)):
    softmax = model.predict(X_train[i, None])
    position = softmax[0][1:].argmax()
    position -= train_shifts[i]
    if position > 0:
        prediction = X_train_raw[i][position]
        if (len(y_train_raw[i]) and prediction == y_train_raw[i][0]):
            correct_count_train += 1
            correct_indices_train += [i]
    total_count_train += 1
    
    
print('train accuracy in \%:', float(correct_count_train)/total_count_train*100)

# predict on whole test dataset (only one word values for now)
total_count_test = 0
correct_count_test = 0
correct_indices_test = []
for i in range(len(X_test)):
    softmax = model.predict(X_test[i, None])
    position = softmax[0][1:].argmax()
    position -= test_shifts[i]
    if position > 0:
        prediction = X_test_raw[i][position]
        if (len(y_test_raw[i]) and prediction == y_test_raw[i][0]):
            correct_count_test += 1
            correct_indices_test += [i]
    total_count_test += 1
    
    
print('test accuracy in \%:', float(correct_count_test)/total_count_test*100)
'''
