import data_parser

import numpy as np
import collections

from gensim.models import Word2Vec
from gensim.models import KeyedVectors

from keras.layers import Embedding
from keras.models import Sequential
from keras.layers import Dropout
from keras.layers import Dense
from keras.layers import LSTM
from keras.preprocessing import sequence
from keras.models import load_model

BASE_DIR = '../011-Common-Files/'
EMBEDDING_FILE = BASE_DIR + 'GoogleNews-vectors-negative300.bin'
TRAIN_DATA_FILE = BASE_DIR + 'trainExamples.json'
TEST_DATA_FILE = BASE_DIR + 'devExamples.json'


# get training data
X_train_raw, y_train_raw = data_parser.get_data(TRAIN_DATA_FILE, MAX_LENGTH = 120)

# get test data
X_test_raw, y_test_raw = data_parser.get_data(TEST_DATA_FILE, MAX_LENGTH = 120)

MAX_SEQUENCE_LENGTH = max([len(X) for X in X_train_raw])
MAX_OUTPUT_LENGTH = max([len(y) for y in y_train_raw+y_test_raw])

# create vocab from data
vocab_data = list(set([j for i in X_train_raw+X_test_raw+y_train_raw+y_test_raw for j in i]))

# load model
model = KeyedVectors.load_word2vec_format(EMBEDDING_FILE, binary=True)
vocab_goog = list(model.vocab.keys())

vocab_used = list(set(vocab_data) & set(vocab_goog))
print ('Length of used vocab: %d' % (len(vocab_used)))
vocab_length = len(vocab_used)

#index vocab
word_index = collections.defaultdict(int)
for i,word in enumerate(vocab_used):
    word_index[word] = i+1 # 0 reserved for Unk
    
# replace word data with index data
X_train = [[word_index[word] for word in description] for description in X_train_raw]
X_test = [[word_index[word] for word in description] for description in X_test_raw]
y_train_i = [[word_index[word] for word in description] for description in y_train_raw]
y_test_i = [[word_index[word] for word in description] for description in y_test_raw]

y_train = np.zeros((len(y_train_i), vocab_length+1)) # think about row/col
indices = [(i,word_idx) for i,v in enumerate(y_train_i) for word_idx in v]
for a,b in indices:
    y_train[a][b] = 1 
    
y_test = np.zeros((len(y_test_i), vocab_length+1)) # think about row/col
indices = [(i,word_idx) for i,v in enumerate(y_test_i) for word_idx in v]
for a,b in indices:
    y_test[a][b] = 1 
        

# pad input data
X_train = sequence.pad_sequences(X_train, maxlen=MAX_SEQUENCE_LENGTH)
X_test = sequence.pad_sequences(X_test, maxlen=MAX_SEQUENCE_LENGTH)



#### CREATE MODEL ####
# create embedding matrix
EMBEDDING_DIM = len(model['this'])
    
embedding_matrix = np.zeros((vocab_length + 1, EMBEDDING_DIM))


for word, idx in word_index.items():
    if word not in model: # TODO: why does this happen
        continue
    embedding_matrix[idx] = model[word]
    
# create keras embedding layer
embedding_layer = Embedding(vocab_length + 1,
                            EMBEDDING_DIM,
                            weights=[embedding_matrix],
                            input_length=MAX_SEQUENCE_LENGTH,
                            trainable=False)


model = Sequential()
model.add(embedding_layer)
model.add(LSTM(MAX_SEQUENCE_LENGTH))
model.add(Dropout(0.2))
model.add(Dense(vocab_length+1, activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
print(model.summary())

#### TRAIN MODEL ####

print('Model is training....')
model.fit(X_train, y_train, epochs=7, batch_size=16)
print('Training finished!')

print('Saving Model...')
model.save('ThirdModelTrained.h5')

print('Evaluating Model...')
scores = model.evaluate(np.squeeze(X_test[:,None]), y_test)

print ('scores:', scores)   


'''

# load model
model = load_model('FirstModelTrained.h5')

test_example = 111
#prediction = model.predict(np.reshape(X_test[test_example], (-1, len(X_test[0]))))
prediction = model.predict(X_test[test_example, None])
print ('max idx Prediction:', prediction[0][1:].argmax())
print ('prediction:', prediction)

'''