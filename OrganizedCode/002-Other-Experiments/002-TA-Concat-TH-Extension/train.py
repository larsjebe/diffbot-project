##### VFL7 ####
## Changes from VFL6
    # normalize y to sum up to 1

import data_parser

import os

import numpy as np
from numpy import linalg as LA
np.random.seed(1337)
import collections

from gensim.models import Word2Vec
from gensim.models import KeyedVectors

from keras.layers import Embedding
from keras.layers import Dropout
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Bidirectional

from keras.preprocessing import sequence
from keras.models import load_model
from keras.models import Sequential

from  more_itertools import unique_everseen

from sklearn.preprocessing import normalize

BASE_DIR = '../011-Common-Files/'
EMBEDDING_FILE = BASE_DIR + 'GoogleNews-vectors-negative300.bin'
TRAIN_DATA_FILE = BASE_DIR + 'trainExamples.json'

#### PARAMETERS ####

LSTM_UNITS = 50
MAX_SEQUENCE_LENGTH = 150
EPOCHS = 30
BATCH_SIZE = 32
TOY_DATASET = True
NUMBER_FILES = 15

MODEL_NAME = 'Toy_30E_32B_50LSTM'

# get training data
if TOY_DATASET:
    X_train_raw, y_train_raw = data_parser.get_predictable_data(TRAIN_DATA_FILE, MAX_LENGTH=MAX_SEQUENCE_LENGTH)

else:
X_train_raw, y_train_raw, _, _ = \
   data_parser.get_more_predictable_data(NUM_FILES = NUMBER_FILES, MAX_LENGTH = MAX_SEQUENCE_LENGTH)


# create vocab from data
vocab_data = [j for i in X_train_raw+y_train_raw for j in i]
vocab_data = list(unique_everseen(vocab_data))

# load model
model = KeyedVectors.load_word2vec_format(EMBEDDING_FILE, binary=True)
vocab_goog = list(model.vocab.keys())

#create detmerinistically ordered vocab
set_goog = frozenset(vocab_goog)
vocab_inters = [x for x in vocab_data if x in set_goog]
part_goog = vocab_goog[:100000]
vocab_used = list(unique_everseen(vocab_inters + part_goog))

print ('Length of used vocab: %d' % (len(vocab_used)))
vocab_length = len(vocab_used)

#index vocab
word_index = collections.defaultdict(int)
for i,word in enumerate(vocab_used):
    word_index[word] = i+1 # 0 reserved for Unk
    
# save the word index for later use
filename = 'word_index_' + MODEL_NAME
if not os.path.exists(filename):
    print ('Saving new word index to file...')
    np.save(filename, word_index)

    
# replace word data with index data
X_train = [[word_index[word] for word in description] for description in X_train_raw]
y_train_i = [[word_index[word] for word in description] for description in y_train_raw]

# convert y_train_i to numpy array
y_train_i = np.array(y_train_i)

# get position of value in text (without padding)
y_train_idx = [[X_train_raw[ex_idx].index(word) if (word in X_train_raw[ex_idx]) else 0 for word in value] for ex_idx, value in enumerate(y_train_raw)]

#shift position by padding (one number per example)
train_shifts = [MAX_SEQUENCE_LENGTH-len(X_train_raw[ex_idx]) for ex_idx, value in enumerate(y_train_raw)]
y_train_idx_shifted = [[el + train_shifts[ex_idx] for el in pos_list] for ex_idx, pos_list in enumerate(y_train_idx)]


# convert y_train_idx_shifted to one-hot (or two-hot etc.)
y_train = np.zeros((len(y_train_i), MAX_SEQUENCE_LENGTH)) 
indices = [(i,word_idx) for i,pos in enumerate(y_train_idx_shifted) for word_idx in pos]
for a,b in indices:
    y_train[a][b] = 1 
    
# normaize to sum of 1 (actually l-2 norm)    
y_train = normalize(y_train, 'l1', axis=1)

        
# pad input data
X_train = sequence.pad_sequences(X_train, maxlen=MAX_SEQUENCE_LENGTH)


#### CREATE MODEL ####
# create embedding matrix
EMBEDDING_DIM = 300 # length of word vectors in google-embedding
    
embedding_matrix = np.zeros((vocab_length + 1, EMBEDDING_DIM))

for word, idx in word_index.items():
    if word not in model: # TODO: why does this happen
        continue
    embedding_matrix[idx] = model[word]
    
# create keras embedding layer
embedding_layer = Embedding(vocab_length + 1,
                            EMBEDDING_DIM,
                            weights=[embedding_matrix],
                            input_length=MAX_SEQUENCE_LENGTH,
                            trainable=False)

model = Sequential()
model.add(embedding_layer)
model.add(LSTM(LSTM_UNITS))
#model.add(Dropout(0.2))
model.add(Dense(MAX_SEQUENCE_LENGTH, activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
print(model.summary())

#### TRAIN MODEL ####

print('Model is training....')
model.fit(X_train, y_train, epochs=30, batch_size=32)
print('Training finished!')

print('Saving Model...')
model.save(MODEL_NAME + '.h5')