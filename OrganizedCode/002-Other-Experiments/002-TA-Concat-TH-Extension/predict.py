import data_parser

import numpy as np
import collections

from gensim.models import Word2Vec
from gensim.models import KeyedVectors

from keras.preprocessing import sequence
from keras.models import load_model

from  more_itertools import unique_everseen

BASE_DIR = '../011-Common-Files/'
EMBEDDING_FILE = BASE_DIR + 'GoogleNews-vectors-negative300.bin'
TRAIN_DATA_FILE = BASE_DIR + 'trainExamples.json'
TEST_DATA_FILE = BASE_DIR + 'devExamples.json'

TRAIN_ERROR = True
TEST_ERROR = True

# Number of LSTM Memory Units (= sequence length)
seq_len = 150

# get training data
X_train_raw, y_train_raw = data_parser.get_predictable_data(TRAIN_DATA_FILE, MAX_LENGTH = seq_len)
# get test data
X_test_raw, y_test_raw = data_parser.get_predictable_data(TEST_DATA_FILE, MAX_LENGTH = seq_len)

#X_train_raw, y_train_raw, X_test_raw, y_test_raw = \
#    data_parser.get_more_predictable_data(NUM_FILES = 50, MAX_LENGTH = 150)

#X_test_raw, y_test_raw = data_parser.get_more_test_data(NUM_FILES = 10, MAX_LENGTH = 150, predictable = False)

MAX_SEQUENCE_LENGTH = seq_len

# load word index from google vocab
word_index = np.load('word_index_toy+100k_bi.npy').item() 
print ('Length of used vocab: %d' % (len(word_index)))
    
# replace words with word indices
if TRAIN_ERROR:
    X_train = [[word_index[word] for word in description] for description in X_train_raw]
    y_train_i = [[word_index[word] for word in description] for description in y_train_raw]
    
    # convert to numpy array
    y_train_i = np.array(y_train_i)
    
    # get position of value in text (without padding)
    y_train_idx = [[X_train_raw[ex_idx].index(word) if (word in X_train_raw[ex_idx]) else 0 for word in value] for ex_idx, value in enumerate(y_train_raw)]
    
    #shift position to account for padding
    train_shifts = [MAX_SEQUENCE_LENGTH-len(X_train_raw[ex_idx]) for ex_idx, value in enumerate(y_train_raw)]
    y_train_idx_shifted = [[el + train_shifts[ex_idx] for el in pos_list] for ex_idx, pos_list in enumerate(y_train_idx)]
    
if TEST_ERROR: 
    X_test = [[word_index[word] for word in description] for description in X_test_raw]
    y_test_i = [[word_index[word] for word in description] for description in y_test_raw]

    X_test_count = sum([word_index[word]>0 for description in X_test_raw for word in description ])
    X_test_total_count = sum([1 for description in X_test_raw for word in description])
    print ('Fraction of known words in X_test:', float(X_test_count)/X_test_total_count)

    y_test_count = sum([word_index[word]>0 for description in y_test_raw for word in description])
    y_test_total_count = sum([1 for description in y_test_raw for word in description])
    print ('Fraction of known words in y_test:', float(y_test_count)/y_test_total_count)

    X_test_att_count = sum([word_index[description[-1]]>1 for description in X_test_raw])
    X_test_att_total_count = sum([1 for description in X_test_raw])
    print ('Fraction of known last words (==att) in X_test:', float(X_test_att_count)/X_test_att_total_count)


    y_test_i = np.array(y_test_i)


    y_test_idx = [[X_test_raw[ex_idx].index(word) if (word in X_test_raw[ex_idx]) else 0 for word in value] for ex_idx, value in enumerate(y_test_raw)]

    test_shifts = [MAX_SEQUENCE_LENGTH-len(X_test_raw[ex_idx]) for ex_idx, value in enumerate(y_test_raw)]
    y_test_idx_shifted = [[el + test_shifts[ex_idx] for el in pos_list] for ex_idx, pos_list in enumerate(y_test_idx)]

if TRAIN_ERROR: 
    # convert y_train_idx_shifted to one-hot (or two-hot if value is longer than 1 word etc.)
    y_train = np.zeros((len(y_train_i), MAX_SEQUENCE_LENGTH)) 
    indices = [(i,word_idx) for i,pos in enumerate(y_train_idx_shifted) for word_idx in pos]
    for a,b in indices:
        y_train[a][b] = 1 
        
    # pad input data
    X_train = sequence.pad_sequences(X_train, maxlen=MAX_SEQUENCE_LENGTH)
    
if TEST_ERROR:
    y_test = np.zeros((len(y_test_i), MAX_SEQUENCE_LENGTH))
    indices = [(i,word_idx) for i,pos in enumerate(y_test_idx_shifted) for word_idx in pos]
    for a,b in indices:
        y_test[a][b] = 1
        
    X_test = sequence.pad_sequences(X_test, maxlen=MAX_SEQUENCE_LENGTH)


model = load_model('Toy_30E_16B_bi.h5')

THRESH_DECAY = 0.45

if TRAIN_ERROR:
# predictions using matrices
    total_count_train = 0
    correct_count_train = 0
    correct_indices_train = []
    correct_count_mult_train = 0
    total_count_mult_train = 0
    softmax = model.predict(np.squeeze(X_train[:, None]))
    position = [softmax[i][0:].argmax() for i in range(np.shape(softmax)[0])]
    position_shifted = [softmax[i][0:].argmax()-train_shifts[i] for i in range(np.shape(softmax)[0])]
    for i in range(len(position)):
        pos_s = position_shifted[i]
        soft = softmax[i]        
        if pos_s >= 0:
            prediction = [X_train_raw[i][pos_s]]
            step = 1
            # append next word to prediction if softmax is confident enough            
            while (position[i]+step < MAX_SEQUENCE_LENGTH) and (soft[position[i]+step] > float(soft[position[i]+step-1])*THRESH_DECAY): 
                prediction.append(X_train_raw[i][pos_s+step])
                step += 1
             
            if prediction == y_train_raw[i]:
                correct_count_train += 1
                correct_indices_train += [i]
                if len(prediction) > 1:
                    correct_count_mult_train += 1
        total_count_train += 1
        if len(y_train_raw[i]) > 1:
            total_count_mult_train += 1
        
    
    print('train accuracy in %:', float(correct_count_train)/total_count_train*100)
    print('train accuracy on multiple word values in %:', float(correct_count_mult_train)/total_count_mult_train*100)

if TEST_ERROR:
    total_count_test = 0
    correct_count_test = 0
    correct_indices_test = []
    correct_count_mult_test = 0
    total_count_mult_test = 0
    predictions =  {}
    softmax = model.predict(np.squeeze(X_test[:, None]))
    position = [softmax[i][0:].argmax() for i in range(np.shape(softmax)[0])]
    position_shifted = [softmax[i][0:].argmax()-test_shifts[i] for i in range(np.shape(softmax)[0])]
    for i in range(len(position)):
        pos_s = position_shifted[i]
        soft = softmax[i]        
        if pos_s >= 0:
            prediction = [X_test_raw[i][pos_s]]
            predictions[i] = prediction
            step = 1
            # append next word to prediction if softmax is confident enough            
            while (position[i]+step < MAX_SEQUENCE_LENGTH) and (soft[position[i]+step] > float(soft[position[i]+step-1])*THRESH_DECAY): 
                prediction.append(X_test_raw[i][pos_s+step])
                step += 1
            step = 1
            # append previous word to prediction if softmax is confident enough            
            while (position[i]-step >= 0) and (soft[position[i]-step] > float(soft[position[i]-step+1])*THRESH_DECAY): 
                prediction.insert(0,X_test_raw[i][pos_s-step])
                step += 1
            if prediction == y_test_raw[i]:
                correct_count_test += 1
                correct_indices_test += [i]
                if len(prediction) > 1:
                    correct_count_mult_test += 1
        total_count_test += 1
        if len(y_test_raw[i]) > 1:
            total_count_mult_test += 1

    print('test accuracy in %:', float(correct_count_test)/total_count_test*100)
    print('test accuracy on multiple word values in %:', float(correct_count_mult_test)/total_count_mult_test*100)

